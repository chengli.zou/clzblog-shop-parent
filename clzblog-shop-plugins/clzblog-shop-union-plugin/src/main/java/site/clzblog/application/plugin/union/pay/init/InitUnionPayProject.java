package site.clzblog.application.plugin.union.pay.init;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import site.clzblog.application.plugin.union.pay.acp.sdk.SDKConfig;

@Component
public class InitUnionPayProject implements ApplicationRunner {

	// springboot 项目启动的时候 执行该方法
	@Override
	public void run(ApplicationArguments args) {
		SDKConfig.getConfig().loadPropertiesFromSrc();
	}
}
