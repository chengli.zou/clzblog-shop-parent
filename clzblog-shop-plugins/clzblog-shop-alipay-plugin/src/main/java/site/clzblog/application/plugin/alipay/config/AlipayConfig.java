package site.clzblog.application.plugin.alipay.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016092700605677";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCJac5NQUoKmR2cJRLIzCuKEQ/HCONjgyKj6bnXw8HTIO3KbOU2Dm7kSm0IuKqIXXJL3BXxyjGbSoJacjamOpTN9tqL1sGE+RCYJ7PiDfDu7DmGfxsPzFcOJBkaLKRugjCLsBpDCb8LQvnTV6Heu3WexskXp5ESnNoWYMqConAIgqQPaSLDeZKPzEiUsWfq4O+t3eZz7ZRndrdsV5/JhtYI4KgGWuG3WAZ6kD9mVMV2Ab0O6U9OnH09q2imz/MNJiO74PhQQ4+6uBst0i4P48Rrs/ceZBAPvNmJv9a9ZSeFkFn7SEQ6zsIGuBc94CLP2DR4Y2XuCjt37Cnq/N5CsHjjAgMBAAECggEAFzcI44RkPeYgWoDo64OS14CKadXWlLlS4N5bx1gLJuApTEmZb8iAc5EfWpnYNbGT3b0Q+Ahw2Qt9ekpRcUP0X9eUlItzpkNtykbq3QX97l96WH6g08px6rGui/1+yiy0huT5itnarMxj6JCliDujY2+yCY0FtV9KeP1hOTC4PLeVgEJoodY/JuEGfnTDWrotLvZP2Zbuvejg6IaP7V1YnnAVqoKTF40LVZLsppi9DOrE/gL2bMAy9seoRYaZ2UaaBiFEO4nr9qF8QgaMXb885ra11bW5VZ8aUy+mUlzVSPUsYWSGmq8Y0QysDBN6KOREq8DmrtBxHNlQu4MJ2gQWAQKBgQDX0RsUu0vWS2G9y803Zin+ytD10VqAQLF0R04ABmH9X4wi77i1lxcREdZxlo86AJJmiha0avZjXw/nhs7qDJUqtVqiGXYq/Z7D1s1nt0bpx77+I4y4mPtY06AurjVfqfoxAaB1B2QJ9VTZZAFXR2/6/SerZhJuKYE5hmJHHIvklwKBgQCi/5oOrHV7OVBWQ9Jj88Edzv4rdUjly/Vh0UIHoA/9wM4izwtKmNoMSPN2p7F7ylc/YR/SBGuoIs0ZPIQCWJolH8oqon+IPYclr71pwuNETmqxqe0wukKdJOAtMSX8QXLkYlQEfaS4EIVX6iijqbz/ZmNBkQ1CpADSUOjk56ublQKBgDnqOH04weIw+nrHXTvXo79nR+ofk0zFj5lnpSvmipdG88FQWV4JvLvs8IIs6CSlkKQJvIwQBCyojwkPqSkv1TvPzRGhCZAB3YAqfZaunU4RN1TVz2aeTx0INX0xbMtOPwC1sgyVvifZ+ToZVy7H0eol02ZwjtOXOcFpKahxpazjAoGAWr4bgRHHsZsdu3i0dwqrtzGWVJFNzQi4CgXaUa5tWKJu3/gN3DCuK2qth6Ah8b2axuEGOwBVAtyk++ixwTCcWBCTlxqyDyHZzERDGc7ygQN66H+mWZtIHH38A4y6De5NzRFknr9n5UTlRyVIDJ0hZ7oThdv/2tq8zllW3qCO6+UCgYB9Q7urz7Em/ZABHIKACA9J0qmj+t4iSR5OGb8maw1r6C1NzewPmzab8FZniU0V7SBWlb0YdmjLhuv++zHAWLmqieFjWTXT/+WsARWwww+fsqwxwDAZo1UXYy1FNnkd0CqtUJiJOdB7K5fIVwB6xi85olLCAW5sENUquU0iGmIROQ==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwuURqnB0/aQT0WMj96Y14R4n8683E+IiFJUQ6mXWSIPHDSB/Qwp19H046Ie3A+CdoY4jD1okiW4wWom5m/BKCeAzk92pQnGId1DaegcrRM80nsVqLhhiSind1/MKi4rZO3eTkegOCPl6Zq53mOMry0ZuysBr6WTPRooJVS2UfzUZTxcsgoYDBzh9ahrMvnx4WexWvJNOz2S4DsZv5C6taZeAV0bW5AHjG3i6ylowENEz6dHHm8mBbmDtfdk1M/lyl2jKQEh/+NGkmGteZQ/MqTVmKZO0AruZn5d/kXjOxi1tqqut8ZhpikTv8OAKyuBYuKViTYaNSiKb810jwqqR4wIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://x4mw3b.natappfree.cc/async/alipay/callback";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://x4mw3b.natappfree.cc/sync/alipay/callback";

    // 签名方式
    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     *
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            String log_path = "/logs";
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis() + ".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
