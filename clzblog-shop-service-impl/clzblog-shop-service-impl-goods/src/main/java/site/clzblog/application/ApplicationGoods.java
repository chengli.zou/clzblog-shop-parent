package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableApolloConfig
@EnableElasticsearchRepositories(basePackages = {"site.clzblog.application.goods.repository"})
public class ApplicationGoods {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationGoods.class, args);
    }
}
