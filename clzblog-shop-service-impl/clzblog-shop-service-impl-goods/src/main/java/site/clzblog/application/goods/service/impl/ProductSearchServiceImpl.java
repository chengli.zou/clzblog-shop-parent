package site.clzblog.application.goods.service.impl;

import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.goods.entity.ProductEntity;
import site.clzblog.application.goods.output.dto.ProductOutputDTO;
import site.clzblog.application.goods.repository.ProductRepository;
import site.clzblog.application.goods.service.ProductSearchService;

import java.util.List;

@RestController
public class ProductSearchServiceImpl extends BaseApiService<List<ProductOutputDTO>> implements ProductSearchService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductSearchServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public BaseResponse<List<ProductOutputDTO>> search(String name, @PageableDefault() Pageable pageable) {
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        builder.must(QueryBuilders.multiMatchQuery(name, "name", "subtitle", "detail"));
        Page<ProductEntity> page = productRepository.search(builder, pageable);
        List<ProductEntity> content = page.getContent();
        DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        List<ProductOutputDTO> map = factory.getMapperFacade().mapAsList(content, ProductOutputDTO.class);
        int i  = 1/0;
        return setResultSuccess(map);
    }
}
