package site.clzblog.application.goods.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import site.clzblog.application.goods.entity.ProductEntity;

@Repository
public interface ProductRepository extends ElasticsearchRepository<ProductEntity, Long> {
}
