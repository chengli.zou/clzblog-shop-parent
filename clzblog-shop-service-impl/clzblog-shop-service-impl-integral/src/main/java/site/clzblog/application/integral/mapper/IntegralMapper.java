package site.clzblog.application.integral.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import site.clzblog.application.integral.entity.IntegralEntity;

public interface IntegralMapper {
    @Insert("insert into `clzblog_integral` values (null, #{userId}, #{paymentId},#{integral}, #{availability}, 0, null, now(), null, now());")
    int insertIntegral(IntegralEntity eiteIntegralEntity);

    @Select("select  id as id ,user_id as userId, payment_id as paymentId ,integral as integral ,availability as availability  from clzblog_integral where payment_id=#{paymentId}  and availability='1';")
    IntegralEntity findIntegral(String paymentId);
}
