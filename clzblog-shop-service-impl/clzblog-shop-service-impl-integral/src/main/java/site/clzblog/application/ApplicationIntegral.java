package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableApolloConfig
@SpringBootApplication
@MapperScan(basePackages = {"site.clzblog.application.integral.mapper"})
public class ApplicationIntegral {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationIntegral.class, args);
    }
}
