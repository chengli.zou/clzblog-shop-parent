package site.clzblog.application.integral.consumer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import site.clzblog.application.integral.entity.IntegralEntity;
import site.clzblog.application.integral.mapper.IntegralMapper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@Component
public class IntegralConsumer {
    private final IntegralMapper integralMapper;

    public IntegralConsumer(IntegralMapper integralMapper) {
        this.integralMapper = integralMapper;
    }

    @RabbitListener(queues = "integral_queue")
    public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws IOException {
        try {
            headers.forEach((key, value) -> log.debug("Rabbit mq headers key -> {}, val -> {}", key, value));
            String messageId = message.getMessageProperties().getMessageId();
            String data = new String(message.getBody(), StandardCharsets.UTF_8);
            log.info("Rabbit mq message id -> {}, data -> {}", messageId, data);
            JSONObject jsonObject = JSONObject.parseObject(data);
            String paymentId = jsonObject.getString("paymentId");
            if (StringUtils.isEmpty(paymentId)) {
                log.warn("Payment id cannot be empty");
                basicNack(message, channel);
                return;
            }

            IntegralEntity integral = integralMapper.findIntegral(paymentId);

            if (null != integral) {
                log.info("Integral added");
                basicNack(message, channel);
                return;
            }

            Integer userId = jsonObject.getInteger("userId");
            if (null == userId) {
                log.error("User id parameter is null");
                basicNack(message, channel);
                return;
            }

            Long value = jsonObject.getLong("integral");
            if (null == value) {
                log.error("Integral parameter is null");
                basicNack(message, channel);
                return;
            }

            IntegralEntity integralEntity = new IntegralEntity();
            integralEntity.setUserId(userId);
            integralEntity.setPaymentId(paymentId);
            integralEntity.setIntegral(value);
            integralEntity.setAvailability(1);
            int i = integralMapper.insertIntegral(integralEntity);
            if (i > 0) {
                basicNack(message, channel);
                log.info("Payment id -> {} add integral successfully", paymentId);
            }
        } catch (Exception e) {
            log.error("Rabbit mq error message -> {}", e.toString());
            basicNack(message, channel);
        }
    }

    private void basicNack(Message message, Channel channel) throws IOException {
        channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
    }
}
