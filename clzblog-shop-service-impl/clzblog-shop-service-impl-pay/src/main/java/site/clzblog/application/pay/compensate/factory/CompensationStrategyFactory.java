package site.clzblog.application.pay.compensate.factory;

import site.clzblog.application.common.core.utils.SpringContextUtil;
import site.clzblog.application.pay.compensate.PaymentCompensationStrategy;

public class CompensationStrategyFactory {
    public static PaymentCompensationStrategy getPaymentCompensationStrategy(String name) {
        return SpringContextUtil.getBean(name, PaymentCompensationStrategy.class);
    }
}
