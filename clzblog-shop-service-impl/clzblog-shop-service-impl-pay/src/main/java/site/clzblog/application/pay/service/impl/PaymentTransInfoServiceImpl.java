package site.clzblog.application.pay.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.pay.output.dto.PymtTransOutDTO;
import site.clzblog.application.pay.service.PaymentTransInfoService;

@RestController
public class PaymentTransInfoServiceImpl extends BaseApiService<PymtTransOutDTO> implements PaymentTransInfoService {
    private final GenerateToken generateToken;

    private final PaymentTransMapper paymentTransMapper;

    @Autowired
    public PaymentTransInfoServiceImpl(GenerateToken generateToken, PaymentTransMapper paymentTransMapper) {
        this.generateToken = generateToken;
        this.paymentTransMapper = paymentTransMapper;
    }

    @Override
    @GetMapping("payment-trans-by/{token}")
    public BaseResponse<PymtTransOutDTO> tokenByPaymentTrans(@PathVariable("token") String token) {
        if (StringUtils.isEmpty(token)) return setResultError("Token cannot be empty");
        String value = generateToken.getToken(token);
        if (StringUtils.isEmpty(value)) return setResultError("Token is expire");
        PymtTransEntity transEntity = paymentTransMapper.selectById(Long.valueOf(value));
        if (transEntity == null) return setResultError("Not found this pay info");
        return setResultSuccess(BeanUtils.doToDto(transEntity, PymtTransOutDTO.class));
    }
}
