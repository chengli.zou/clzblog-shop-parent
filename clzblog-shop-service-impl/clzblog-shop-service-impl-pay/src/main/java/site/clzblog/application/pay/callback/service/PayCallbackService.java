package site.clzblog.application.pay.callback.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.pay.callback.template.factory.TemplateFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class PayCallbackService {
    private static final String UNION_PAY_CALLBACK_TEMPLATE = "unionPayCallbackTemplate";

    private static final String ALIPAY_CALLBACK_TEMPLATE = "alipayCallbackTemplate";

    @RequestMapping("async/union-pay/callback")
    public String asyncUnionPayCallback(HttpServletRequest request, HttpServletResponse response) {
        return TemplateFactory.getPayCallbackTemplate(UNION_PAY_CALLBACK_TEMPLATE).callbackAsync(request, response);
    }

    @RequestMapping("async/alipay/callback")
    public String asyncAlipayCallback(HttpServletRequest request, HttpServletResponse response) {
        return TemplateFactory.getPayCallbackTemplate(ALIPAY_CALLBACK_TEMPLATE).callbackAsync(request, response);
    }

    @RequestMapping("sync/alipay/callback")
    public String syncAlipayCallback(HttpServletRequest request) {
        return TemplateFactory.getPayCallbackTemplate(ALIPAY_CALLBACK_TEMPLATE).callbackSync(request);
    }
}
