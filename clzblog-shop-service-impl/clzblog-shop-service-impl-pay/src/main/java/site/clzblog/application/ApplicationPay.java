package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableApolloConfig
@MapperScan(basePackages = "site.clzblog.application.pay.mapper")
public class ApplicationPay {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationPay.class, args);
    }
}
