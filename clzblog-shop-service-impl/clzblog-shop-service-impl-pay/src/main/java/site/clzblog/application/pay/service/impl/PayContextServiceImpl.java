package site.clzblog.application.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.pay.entity.PymtChlEntity;
import site.clzblog.application.pay.mapper.PaymentChannelMapper;
import site.clzblog.application.pay.output.dto.PymtTransOutDTO;
import site.clzblog.application.pay.service.PayContextService;
import site.clzblog.application.pay.service.PaymentTransInfoService;
import site.clzblog.application.pay.strategy.PayStrategy;
import site.clzblog.application.pay.strategy.factory.StrategyFactory;

@RestController
public class PayContextServiceImpl extends BaseApiService<JSONObject> implements PayContextService {
    private final PaymentChannelMapper paymentChannelMapper;
    private final PaymentTransInfoService paymentTransInfoService;

    @Autowired
    public PayContextServiceImpl(PaymentChannelMapper paymentChannelMapper, PaymentTransInfoService paymentTransInfoService) {
        this.paymentChannelMapper = paymentChannelMapper;
        this.paymentTransInfoService = paymentTransInfoService;
    }

    @Override
    public BaseResponse<JSONObject> toPayHtml(String channelId, String payToken) {
        PymtChlEntity pymtChlEntity = paymentChannelMapper.selectByChannelId(channelId);

        if (null == pymtChlEntity) return setResultError("The channel info not found");

        BaseResponse<PymtTransOutDTO> trans = paymentTransInfoService.tokenByPaymentTrans(payToken);

        if (!isSuccess(trans)) setResultError(trans.getMsg());

        PayStrategy payStrategy = StrategyFactory.getPayStrategy(pymtChlEntity.getClassAddr());
        if (null == payStrategy) return setResultError("Payment system gateway error");

        JSONObject data = new JSONObject();
        data.put("payHtml", payStrategy.toPayHtml(pymtChlEntity, trans.getData()));

        return setResultSuccess(data);
    }
}
