package site.clzblog.application.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.pay.compensate.PaymentCompensationStrategy;
import site.clzblog.application.pay.compensate.factory.CompensationStrategyFactory;
import site.clzblog.application.pay.entity.PymtChlEntity;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentChannelMapper;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.pay.service.PaymentCompensationService;

import java.util.List;

@RestController
public class PaymentCompensationServiceImpl extends BaseApiService<JSONObject> implements PaymentCompensationService {
    private final PaymentTransMapper paymentTransMapper;

    private final PaymentChannelMapper paymentChannelMapper;

    @Autowired
    public PaymentCompensationServiceImpl(PaymentTransMapper paymentTransMapper, PaymentChannelMapper paymentChannelMapper) {
        this.paymentTransMapper = paymentTransMapper;
        this.paymentChannelMapper = paymentChannelMapper;
    }

    @Override
    @RequestMapping("payment-compensation/{payment-id}")
    public BaseResponse<JSONObject> paymentCompensation(@PathVariable("payment-id") String paymentId) {
        if (StringUtils.isEmpty(paymentId)) return setResultError("Payment id cannot be empty");

        PymtTransEntity pymtTransEntity = paymentTransMapper.selectByPaymentId(paymentId);
        if (null == pymtTransEntity) return setResultError("Payment transaction is empty");

        BaseResponse<JSONObject> response = null;
        List<PymtChlEntity> pymtChlEntities = paymentChannelMapper.selectAll();
        for (PymtChlEntity channel : pymtChlEntities) {
            if (null != channel) response = compensationStrategy(pymtTransEntity, channel);
        }

        return null == response ? setResultError("Not execute retry jobs") : response;
    }

    private BaseResponse<JSONObject> compensationStrategy(PymtTransEntity pymtTransEntity, PymtChlEntity channel) {
        PaymentCompensationStrategy strategy = CompensationStrategyFactory.getPaymentCompensationStrategy(channel.getRetryBeanId());

        Boolean compensation = strategy.paymentCompensation(pymtTransEntity, channel);

        return compensation ? setResultSuccess("Retry success") : setResultError("Retry failure");
    }
}
