package site.clzblog.application.pay.compensate.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeQueryRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.pay.compensate.PaymentCompensationStrategy;
import site.clzblog.application.pay.constant.PayConstants;
import site.clzblog.application.pay.entity.PymtChlEntity;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.plugin.alipay.config.AlipayConfig;

@Slf4j
@Component
public class AlipayCompensationStrategy extends BaseApiService<JSONObject> implements PaymentCompensationStrategy {

    private final PaymentTransMapper paymentTransMapper;

    @Autowired
    public AlipayCompensationStrategy(PaymentTransMapper paymentTransMapper) {
        this.paymentTransMapper = paymentTransMapper;
    }

    @Override
    public Boolean paymentCompensation(PymtTransEntity pymtTransEntity, PymtChlEntity pymtChlEntity) {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeQueryRequest alipayRequest = new AlipayTradeQueryRequest();

        JSONObject data = new JSONObject();
        String paymentId = pymtTransEntity.getPaymentId();
        data.put("out_trade_no", pymtTransEntity.getPaymentId());

        alipayRequest.setBizContent(data.toJSONString());

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            log.info("Alipay retry order status result -> {}", result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        Object tradeStatus = JSONObject.parseObject(result).get("trade_status");
        String trade_status = null == tradeStatus ? "" : tradeStatus.toString();

        if (!(PayConstants.TRADE_SUCCESS.equals(trade_status) || PayConstants.TRADE_FINISHED.equals(trade_status))) {
            return false;
        } else {
            paymentTransMapper.updatePaymentStatus(PayConstants.PAY_STATUS_SUCCESS.toString(), paymentId, PayConstants.ALIPAY);
            return true;
        }
    }
}
