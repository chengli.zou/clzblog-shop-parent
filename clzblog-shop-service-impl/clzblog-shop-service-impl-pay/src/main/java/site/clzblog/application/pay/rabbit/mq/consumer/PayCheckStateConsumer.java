package site.clzblog.application.pay.rabbit.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import site.clzblog.application.pay.constant.PayConstants;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.pay.rabbit.config.RabbitMQConfig;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@Component
public class PayCheckStateConsumer {
    private final PaymentTransMapper paymentTransMapper;

    public PayCheckStateConsumer(PaymentTransMapper paymentTransMapper) {
        this.paymentTransMapper = paymentTransMapper;
    }

    @RabbitListener(queues = RabbitMQConfig.INTEGRAL_COMPENSATE_QUEUE)
    public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws IOException {
        headers.forEach((key, value) -> log.info("Rabbit mq message header key -> {}, val -> {}", key, value));
        String messageId = message.getMessageProperties().getMessageId();
        String data = new String(message.getBody(), StandardCharsets.UTF_8);
        log.info("Rabbit mq message id -> {}, data -> {}", messageId, data);
        try {
            JSONObject jsonObject = JSONObject.parseObject(data);
            String paymentId = jsonObject.getString("paymentId");
            if (StringUtils.isEmpty(paymentId)) {
                log.error("Payment id cannot be empty");
                basicNack(message, channel);
                return;
            }

            PymtTransEntity pymtTransEntity = paymentTransMapper.selectByPaymentId(paymentId);
            if (null == pymtTransEntity) {
                log.warn("According payment id -> {} not found", paymentId);
                basicNack(message, channel);
                return;
            }

            Integer paymentStatus = pymtTransEntity.getPaymentStatus();
            if (PayConstants.PAY_STATUS_SUCCESS.equals(paymentStatus)) {
                log.info("Payment succeed");
                basicNack(message, channel);
                return;
            }

            String paymentChannel = jsonObject.getString("paymentChannel");
            int i = paymentTransMapper.updatePaymentStatus(PayConstants.PAY_STATUS_SUCCESS.toString(), paymentId, paymentChannel);
            if (i > 0) {
                basicNack(message, channel);
            }
        } catch (Exception e) {
            e.printStackTrace();
            basicNack(message, channel);
        }
    }

    private void basicNack(Message message, Channel channel) throws IOException {
        channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
    }
}
