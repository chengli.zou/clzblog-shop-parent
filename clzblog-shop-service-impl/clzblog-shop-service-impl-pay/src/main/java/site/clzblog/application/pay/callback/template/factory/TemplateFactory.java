package site.clzblog.application.pay.callback.template.factory;

import site.clzblog.application.common.core.utils.SpringContextUtil;
import site.clzblog.application.pay.callback.template.AbstractPayCallbackTemplate;

public class TemplateFactory {
    public static AbstractPayCallbackTemplate getPayCallbackTemplate(String beanId) {
        return SpringContextUtil.getBean(beanId, AbstractPayCallbackTemplate.class);
    }
}
