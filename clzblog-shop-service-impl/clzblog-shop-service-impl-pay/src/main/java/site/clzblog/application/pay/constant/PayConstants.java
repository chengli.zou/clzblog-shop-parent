package site.clzblog.application.pay.constant;

public interface PayConstants {
    String RESULT_NAME = "result";
    String RESULT_PAY_CODE_201 = "201";
    String RESULT_PAY_CODE_200 = "200";
    Integer PAY_STATUS_SUCCESS = 1;
    String UNION_PAY = "union_pay";
    String UNION_RESULT_OK = "ok";
    String UNION_RESULT_FAIL = "fail";
    String TRADE_FINISHED = "TRADE_FINISHED";
    String TRADE_SUCCESS = "TRADE_SUCCESS";
    String ALIPAY = "alipay";
    String ALIPAY_RESULT_SUCCESS = "success";
    String ALIPAY_RESULT_FAILURE = "fail";
}
