package site.clzblog.application.pay.strategy.factory;

import site.clzblog.application.pay.strategy.PayStrategy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StrategyFactory {
    private static Map<String, PayStrategy> strategyMap = new ConcurrentHashMap<>();

    public static PayStrategy getPayStrategy(String classAddr) {
        PayStrategy payStrategy = null;
        try {
            PayStrategy beanPayStrategy = strategyMap.get(classAddr);
            if (beanPayStrategy != null) return beanPayStrategy;
            Class<?> aClass = Class.forName(classAddr);
            Object o = aClass.newInstance();
            if (o instanceof PayStrategy) payStrategy = (PayStrategy) o;
            strategyMap.put(classAddr, payStrategy);
            return payStrategy;
        } catch (Exception e) {
            return null;
        }
    }
}
