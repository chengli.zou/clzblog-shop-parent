package site.clzblog.application.pay.mapper;

import org.apache.ibatis.annotations.Select;
import site.clzblog.application.pay.entity.PymtChlEntity;

import java.util.List;

public interface PaymentChannelMapper {
    @Select("SELECT channel_name  AS channelName , channel_id AS channelId, merchant_id AS merchantId,sync_Url AS syncUrl, async_url AS asyncUrl,public_key AS publicKey, private_key AS privateKey,channel_state AS channelState ,class_addr as classAddr,retry_bean_id as retryBeanId  FROM payment_channel WHERE channel_state='0';")
    List<PymtChlEntity> selectAll();

    @Select("SELECT channel_name  AS channelName , channel_id AS channelId, merchant_id AS merchantId,sync_Url AS syncUrl, async_url AS asyncUrl,public_key AS publicKey, private_key AS privateKey,channel_state AS channelState ,class_addr as classAddr,retry_bean_id as retryBeanId  FROM payment_channel WHERE channel_state='0'  AND channel_Id=#{channelId} ;")
    PymtChlEntity selectByChannelId(String channelId);
}
