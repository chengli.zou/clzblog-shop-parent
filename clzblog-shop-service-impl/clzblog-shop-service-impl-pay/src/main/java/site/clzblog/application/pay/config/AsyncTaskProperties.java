package site.clzblog.application.pay.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "async.task.config")
public class AsyncTaskProperties {
    private int corePoolSize;
    private int maxPoolSize;
    private int queueCapacity;
}
