package site.clzblog.application.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.common.twitter.util.SnowflakeIdUtil;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.input.dto.CreatePayTokenInDTO;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.pay.service.PaymentTransTokenService;

@RestController
public class PaymentTransTokenServiceImpl extends BaseApiService<JSONObject> implements PaymentTransTokenService {
    private final PaymentTransMapper paymentTransMapper;

    private final GenerateToken generateToken;

    @Autowired
    public PaymentTransTokenServiceImpl(PaymentTransMapper paymentTransMapper, GenerateToken generateToken) {
        this.paymentTransMapper = paymentTransMapper;
        this.generateToken = generateToken;
    }

    @Override
    public BaseResponse<JSONObject> createPayToken(CreatePayTokenInDTO createPayTokenInDTO) {
        String orderId = createPayTokenInDTO.getOrderId();
        if (StringUtils.isEmpty(orderId)) return setResultError("Order id cannot be empty");

        Long payAmount = createPayTokenInDTO.getPayAmount();
        if (null == payAmount) return setResultError("Amount cannot be null");

        Long userId = createPayTokenInDTO.getUserId();
        if (null == userId) return setResultError("User id cannot be null");

        PymtTransEntity pymtTransEntity = new PymtTransEntity();
        pymtTransEntity.setOrderId(orderId);
        pymtTransEntity.setPayAmount(payAmount);
        pymtTransEntity.setUserId(userId);
        // Use twitter snowflake algorithm generate global id
        pymtTransEntity.setPaymentId(SnowflakeIdUtil.nextId());

        int i = paymentTransMapper.insertPaymentTransaction(pymtTransEntity);
        if (!toDaoResult(i)) return setResultError("System error");

        Long id = pymtTransEntity.getId();
        if (null == id) return setResultError("System error");

        String token = generateToken.createToken("pay_", id.toString());

        JSONObject data = new JSONObject();
        data.put("token", token);

        return setResultSuccess(data);
    }
}
