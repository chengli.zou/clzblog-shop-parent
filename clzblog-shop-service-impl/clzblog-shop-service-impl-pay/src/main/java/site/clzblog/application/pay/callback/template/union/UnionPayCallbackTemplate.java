package site.clzblog.application.pay.callback.template.union;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.application.pay.callback.template.AbstractPayCallbackTemplate;
import site.clzblog.application.pay.constant.PayConstants;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.pay.rabbit.mq.producer.IntegralProducer;
import site.clzblog.application.plugin.union.pay.acp.sdk.AcpService;
import site.clzblog.application.plugin.union.pay.acp.sdk.LogUtil;
import site.clzblog.application.plugin.union.pay.acp.sdk.SDKConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
public class UnionPayCallbackTemplate extends AbstractPayCallbackTemplate {
    private final IntegralProducer integralProducer;

    private PaymentTransMapper paymentTransMapper;

    @Autowired
    public UnionPayCallbackTemplate(PaymentTransMapper paymentTransMapper, IntegralProducer integralProducer) {
        this.paymentTransMapper = paymentTransMapper;
        this.integralProducer = integralProducer;
    }

    @Override
    protected String asyncService(Map<String, String> verify) {
        String orderId = verify.get("orderId"); // 获取后台通知的数据，其他字段也可用类似方式获取
        String respCode = verify.get("respCode");

        // 判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
        System.out.println("orderId:" + orderId + ",respCode:" + respCode);
        // 1.判断respCode是否为已经支付成功断respCode=00、A6后，
        if (!(respCode.equals("00") || respCode.equals("A6"))) {
            return failure();
        }
        // 根据日志 手动补偿 使用支付id调用第三方支付接口查询
        PymtTransEntity paymentTransaction = paymentTransMapper.selectByPaymentId(orderId);
        if (paymentTransaction.getPaymentStatus().equals(PayConstants.PAY_STATUS_SUCCESS)) {
            // 网络重试中，之前已经支付过
            return success();
        }
        // 2.将状态改为已经支付成功
        paymentTransMapper.updatePaymentStatus(PayConstants.PAY_STATUS_SUCCESS.toString(), orderId, PayConstants.UNION_PAY);

        // 3.调用积分服务接口增加积分(处理幂等性问题)
        asyncAddIntegral(paymentTransaction);

        return success();
    }

    @Override
    protected String syncService(HttpServletRequest request) {
        return null;
    }

    @Override
    protected String failure() {
        return PayConstants.UNION_RESULT_FAIL;
    }

    @Override
    protected String success() {
        return PayConstants.UNION_RESULT_OK;
    }

    @Override
    protected Map<String, String> verifySignature(HttpServletRequest request, HttpServletResponse response) {
        LogUtil.writeLog("BackRcvResponse接收后台通知开始");

        String encoding = request.getParameter(SDKConstants.param_encoding);
        // 获取银联通知服务器发送的后台通知参数
        Map<String, String> reqParam = getAllRequestParams(request);
        LogUtil.printRequestLog(reqParam);

        // 重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
        if (!AcpService.validate(reqParam, encoding)) {
            LogUtil.writeLog("验证签名结果[失败].");
            reqParam.put(PayConstants.RESULT_NAME, PayConstants.RESULT_PAY_CODE_201);
        } else {
            LogUtil.writeLog("验证签名结果[成功].");
            // 【注：为了安全验签成功才应该写商户的成功处理逻辑】交易成功，更新商户订单状态
            String orderId = reqParam.get("orderId"); // 获取后台通知的数据，其他字段也可用类似方式获取
            reqParam.put("paymentId", orderId);
            reqParam.put(PayConstants.RESULT_NAME, PayConstants.RESULT_PAY_CODE_200);
        }
        LogUtil.writeLog("BackRcvResponse接收后台通知结束");
        return reqParam;
    }

    private static Map<String, String> getAllRequestParams(final HttpServletRequest request) {
        Map<String, String> res = new HashMap<>();
        Enumeration<?> temp = request.getParameterNames();
        if (null != temp) {
            while (temp.hasMoreElements()) {
                String en = temp.nextElement().toString();
                String value = request.getParameter(en);
                res.put(en, value);
                if (res.get(en) == null || "".equals(res.get(en))) res.remove(en);
            }
        }
        return res;
    }

    @Async
    public void asyncAddIntegral(PymtTransEntity paymentTransaction) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("paymentId", paymentTransaction.getPaymentId());
        jsonObject.put("userId", paymentTransaction.getUserId());
        jsonObject.put("integral", "100");
        integralProducer.send(jsonObject);
    }
}
