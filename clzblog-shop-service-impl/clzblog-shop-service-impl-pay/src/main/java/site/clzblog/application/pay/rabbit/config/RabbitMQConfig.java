package site.clzblog.application.pay.rabbit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConfig {
    public static final String INTEGRAL_EXCHANGE = "integral_exchange";
    public static final String INTEGRAL_ROUTING_KEY = "integral_routing_key";
    private static final String INTEGRAL_QUEUE = "integral_queue";
    public static final String INTEGRAL_COMPENSATE_QUEUE = "integral_compensate_queue";

    @Bean
    public Queue integralQueue() {
        return new Queue(INTEGRAL_QUEUE);
    }

    @Bean
    public Queue integralCompensateQueue() {
        return new Queue(INTEGRAL_COMPENSATE_QUEUE);
    }

    @Bean
    DirectExchange integralDirectExchange() {
        return new DirectExchange(INTEGRAL_EXCHANGE);
    }

    @Bean
    Binding bindingIntegralQueue() {
        return BindingBuilder.bind(integralQueue()).to(integralDirectExchange()).with(INTEGRAL_ROUTING_KEY);
    }

    @Bean
    Binding bindingIntegralCompensateQueue() {
        return BindingBuilder.bind(integralCompensateQueue()).to(integralDirectExchange()).with(INTEGRAL_ROUTING_KEY);
    }
}
