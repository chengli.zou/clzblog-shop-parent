package site.clzblog.application.pay.rabbit.mq.producer;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.application.pay.rabbit.config.RabbitMQConfig;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class IntegralProducer implements RabbitTemplate.ConfirmCallback {
    private final RabbitTemplate rabbitTemplate;

    public IntegralProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Transactional
    public void send(JSONObject jsonObject) {
        String jsonString = jsonObject.toJSONString();
        Message message = MessageBuilder.withBody(jsonString.getBytes())
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .setContentEncoding(StandardCharsets.UTF_8.toString())
                .setMessageId(jsonObject.getString("paymentId")).build();
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setConfirmCallback(this);
        CorrelationData data = new CorrelationData(jsonString);
        rabbitTemplate.convertAndSend(RabbitMQConfig.INTEGRAL_EXCHANGE, RabbitMQConfig.INTEGRAL_ROUTING_KEY, message, data);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String s) {
        log.info("Rabbit mq message id -> {}", correlationData.getId());
        if (ack) log.info("Rabbit mq message acknowledged");
        else send(JSONObject.parseObject(s));
    }
}
