package site.clzblog.application.pay.callback.template;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.application.pay.constant.PayConstants;
import site.clzblog.application.pay.entity.PymtTransLogEntity;
import site.clzblog.application.pay.mapper.PaymentTransLogMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Slf4j
@Component
public abstract class AbstractPayCallbackTemplate {
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private PaymentTransLogMapper paymentTransLogMapper;

    @Transactional
    public String callbackAsync(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> verify = verifySignature(request, response);
        String paymentId = verify.get("paymentId");

        if (StringUtils.isEmpty(paymentId)) return failure();

        taskExecutor.execute(() -> asyncWriteLogToDB(paymentId, verify));

        if (PayConstants.RESULT_PAY_CODE_201.equals(verify.get(PayConstants.RESULT_NAME))) return failure();

        return asyncService(verify);
    }


    public String callbackSync(HttpServletRequest request) {
        return syncService(request);
    }

    protected abstract String asyncService(Map<String, String> verify);

    protected abstract String syncService(HttpServletRequest request);

    private void asyncWriteLogToDB(String paymentId, Map<String, String> verify) {
        log.info("PaymentId -> {},Verify params -> {}", paymentId, verify);
        PymtTransLogEntity logEntity = new PymtTransLogEntity();
        logEntity.setTransactionId(paymentId);
        logEntity.setAsyncLog(verify.toString());
        paymentTransLogMapper.insertTransLog(logEntity);
    }

    protected abstract String failure();

    protected abstract String success();

    protected abstract Map<String, String> verifySignature(HttpServletRequest request, HttpServletResponse response);
}
