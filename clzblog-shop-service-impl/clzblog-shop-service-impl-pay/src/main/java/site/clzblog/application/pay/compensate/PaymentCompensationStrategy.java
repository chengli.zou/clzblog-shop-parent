package site.clzblog.application.pay.compensate;

import site.clzblog.application.pay.entity.PymtChlEntity;
import site.clzblog.application.pay.entity.PymtTransEntity;

public interface PaymentCompensationStrategy {
    Boolean paymentCompensation(PymtTransEntity pymtTransEntity, PymtChlEntity pymtChlEntity);
}
