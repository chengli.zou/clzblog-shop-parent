package site.clzblog.application.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.pay.rabbit.mq.producer.IntegralProducer;

@RestController
public class RabbitMQTestServiceImpl {
    private final IntegralProducer integralProducer;

    public RabbitMQTestServiceImpl(IntegralProducer integralProducer) {
        this.integralProducer = integralProducer;
    }

    @PostMapping("send")
    public String send() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("paymentId", System.currentTimeMillis());
        jsonObject.put("userId", "123456");
        jsonObject.put("integral", "100");
        integralProducer.send(jsonObject);
        return "success";
    }
}
