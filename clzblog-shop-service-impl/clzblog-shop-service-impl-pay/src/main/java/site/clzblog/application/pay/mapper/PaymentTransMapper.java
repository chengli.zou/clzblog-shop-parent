package site.clzblog.application.pay.mapper;

import org.apache.ibatis.annotations.*;
import site.clzblog.application.pay.entity.PymtTransEntity;

public interface PaymentTransMapper {
    @Options(useGeneratedKeys = true)
    @Insert("INSERT INTO `payment_transaction` VALUES (null, #{payAmount}, '0', #{userId}, #{orderId}, null, null, now(), null, now(),null,#{paymentId},#{paymentChannel});")
    int insertPaymentTransaction(PymtTransEntity pymtTransEntity);

    @Select("SELECT id AS ID ,pay_amount AS payAmount,payment_status AS paymentStatus,user_id AS userId, order_id AS orderId , created_time as createdTime ,party_pay_id as partyPayId , payment_id as paymentId FROM payment_transaction WHERE id=#{id};")
    PymtTransEntity selectById(Long id);

    @Select("SELECT id AS id ,pay_amount AS payAmount,payment_status AS paymentStatus,user_id AS userId, order_id AS orderId , created_time as createdTime ,party_pay_id as partyPayId , payment_id as paymentId FROM payment_transaction WHERE payment_id=#{paymentId};")
    PymtTransEntity selectByPaymentId(String paymentId);

    @Update("update payment_transaction SET payment_status=#{paymentStatus},payment_channel=#{paymentChannel}  WHERE payment_id=#{paymentId};")
    int updatePaymentStatus(@Param("paymentStatus") String paymentStatus, @Param("paymentId") String paymentId, @Param("paymentChannel") String paymentChannel);
}
