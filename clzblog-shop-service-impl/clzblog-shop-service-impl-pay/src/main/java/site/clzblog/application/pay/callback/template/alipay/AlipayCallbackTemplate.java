package site.clzblog.application.pay.callback.template.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.sign.SignUtils;
import site.clzblog.application.pay.callback.template.AbstractPayCallbackTemplate;
import site.clzblog.application.pay.constant.PayConstants;
import site.clzblog.application.pay.entity.PymtTransEntity;
import site.clzblog.application.pay.mapper.PaymentTransMapper;
import site.clzblog.application.plugin.alipay.config.AlipayConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@Component
public class AlipayCallbackTemplate extends AbstractPayCallbackTemplate {
    private final PaymentTransMapper paymentTransMapper;

    @Autowired
    public AlipayCallbackTemplate(PaymentTransMapper paymentTransMapper) {
        this.paymentTransMapper = paymentTransMapper;
    }

    @Override
    protected String asyncService(Map<String, String> verify) {
        String orderId = verify.get("outTradeNo");
        String status = verify.get("tradeStatus");

        if (!(PayConstants.TRADE_SUCCESS.equals(status) || PayConstants.TRADE_FINISHED.equals(status)))
            return failure();

        PymtTransEntity trans = paymentTransMapper.selectByPaymentId(orderId);
        if (trans.getPaymentStatus().equals(PayConstants.PAY_STATUS_SUCCESS)) return success();

        paymentTransMapper.updatePaymentStatus(PayConstants.PAY_STATUS_SUCCESS.toString(), orderId, PayConstants.ALIPAY);

        return success();
    }

    @Override
    protected String syncService(HttpServletRequest request) {
        Map<String, String> params = SignUtils.toVerifyMap(request.getParameterMap(), true);
        boolean signVerified = isSignVerified(params);

        //——请在这里编写您的程序（以下代码仅作参考）——
        if (signVerified) {
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            //付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            log.info("trade_no:" + trade_no + "<br/>out_trade_no:" + out_trade_no + "<br/>total_amount:" + total_amount);
        } else {
            log.info("Synchronized verify signature failure");
        }
        return signVerified ? "success" : "failure";
    }

    private boolean isSignVerified(Map<String, String> params) {
        boolean signVerified = false; //调用SDK验证签名
        try {
            signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return signVerified;
    }

    @Override
    protected String failure() {
        return PayConstants.ALIPAY_RESULT_FAILURE;
    }

    @Override
    protected String success() {
        return PayConstants.ALIPAY_RESULT_SUCCESS;
    }

    @Override
    protected Map<String, String> verifySignature(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> params = SignUtils.toVerifyMap(request.getParameterMap(), true);
        boolean signVerified = isSignVerified(params);

        /* 实际验证过程建议商户务必添加以下校验：
        1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
        4、验证app_id是否为该商户本身。
        */
        if (signVerified) {//验证成功
            log.info("Alipay validate signature success");
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            params.put("tradeStatus", trade_status);
            params.put("tradeNo", trade_no);
            params.put("outTradeNo", out_trade_no);
            params.put("paymentId", out_trade_no);

            params.put(PayConstants.RESULT_NAME, PayConstants.RESULT_PAY_CODE_200);
        } else {//验证失败
            log.info("Alipay validate signature failure");
            params.put(PayConstants.RESULT_NAME, PayConstants.RESULT_PAY_CODE_201);
        }
        return params;
    }

}
