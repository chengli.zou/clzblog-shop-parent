package site.clzblog.application.pay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.mapper.util.MapperUtil;
import site.clzblog.application.pay.mapper.PaymentChannelMapper;
import site.clzblog.application.pay.output.dto.PymtChlOutDTO;
import site.clzblog.application.pay.service.PaymentChannelService;

import java.util.List;

@RestController
public class PaymentChannelServiceImpl extends BaseApiService<List<PymtChlOutDTO>> implements PaymentChannelService {
    private final PaymentChannelMapper paymentChannelMapper;

    @Autowired
    public PaymentChannelServiceImpl(PaymentChannelMapper paymentChannelMapper) {
        this.paymentChannelMapper = paymentChannelMapper;
    }

    @Override
    public List<PymtChlOutDTO> selectAll() {
        return MapperUtil.mapAsList(paymentChannelMapper.selectAll(), PymtChlOutDTO.class);
    }
}
