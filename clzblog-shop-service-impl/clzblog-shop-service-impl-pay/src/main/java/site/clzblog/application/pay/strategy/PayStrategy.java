package site.clzblog.application.pay.strategy;

import site.clzblog.application.pay.entity.PymtChlEntity;
import site.clzblog.application.pay.output.dto.PymtTransOutDTO;

public interface PayStrategy {
    String toPayHtml(PymtChlEntity pymtChlEntity, PymtTransOutDTO pymtTransOutDTO);
}
