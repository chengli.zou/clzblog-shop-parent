package site.clzblog.application.pay.mapper;

import org.apache.ibatis.annotations.Insert;
import site.clzblog.application.pay.entity.PymtTransLogEntity;

public interface PaymentTransLogMapper {
    @Insert("insert into payment_transaction_log values (null,null,#{asyncLog},null,#{transactionId},null,null,now(),null,now())")
    int insertTransLog(PymtTransLogEntity pymtTransLogEntity);
}
