package site.clzblog.application.spike.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.spike.mapper.SecKillMapper;
import site.clzblog.application.spike.mapper.entity.SecKillEntity;
import site.clzblog.application.spike.rabbit.mq.producer.SpikeCommodityProducer;
import site.clzblog.application.spike.service.SpikeCommodityService;

@Slf4j
@RestController
public class SpikeCommodityServiceImpl extends BaseApiService<JSONObject> implements SpikeCommodityService {
    private final SpikeCommodityProducer spikeCommodityProducer;

    private final GenerateToken generateToken;

    private final SecKillMapper secKillMapper;

    public SpikeCommodityServiceImpl(SecKillMapper secKillMapper, GenerateToken generateToken, SpikeCommodityProducer spikeCommodityProducer) {
        this.secKillMapper = secKillMapper;
        this.generateToken = generateToken;
        this.spikeCommodityProducer = spikeCommodityProducer;
    }

    public BaseResponse<JSONObject> spikeFallback(String phone, Long secKillId) {
        return setResultError("Server are too busy Please try again later");
    }

    @Override
    @HystrixCommand(fallbackMethod = "spikeFallback")
    public BaseResponse<JSONObject> spike(String phone, Long secKillId) {
        if (StringUtils.isEmpty(phone)) return setResultError("User phone cannot be empty");

        if (null == secKillId) return setResultError("Commodity inventory id cannot be empty");

        String tokensByKey = generateToken.getTokensByKey(secKillId.toString());
        if (StringUtils.isEmpty(tokensByKey)) {
            log.info("Dear The shop {} sold out", secKillId);
            return setResultError("Dear The shop sold out");
        }

        sendSecKillMsg(secKillId, phone);

        /*SecKillEntity entity = secKillMapper.selectBySecKillId(secKillId);
        if (null == entity) return setResultError("Commodity info don't exists");

        Boolean setNx = redisUtils.setNx(phone, secKillId.toString(), 10L);
        if (!setNx) return setResultError("Visit frequency excessive");

        int i = secKillMapper.inventoryDeduction(secKillId, entity.getVersion());
        if (!toDaoResult(i)) {
            log.info("====================>> Inventory deduction failure.return result -> {} <<====================", i);
            return setResultError("Dear you please try again later!");
        }

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setUserPhone(phone);
        orderEntity.setSecKillId(secKillId);
        int order = orderMapper.insertOrder(orderEntity);
        if (!toDaoResult(order)) return setResultError("Dear you please try again later!");

        log.info("====================>> Inventory deduction success.return result -> {} <<====================", i);*/

        return setResultSuccess("Being queued");
    }

    @Override
    public BaseResponse<JSONObject> addSpikeToken(Long secKillId, Long tokenQuantity) {
        if (null == secKillId) return setResultError("Commodity inventory id cannot be null");

        if (null == tokenQuantity) return setResultError("Token quantity cannot be null");

        SecKillEntity entity = secKillMapper.selectBySecKillId(secKillId);
        if (null == entity) return setResultError("Commodity info don't exists");

        createSecKillTokens(secKillId, tokenQuantity);

        return setResultSuccess("Token generating ...");
    }

    @Async
    void createSecKillTokens(Long secKillId, Long tokenQuantity) {
        generateToken.createTokens("sec_kill_", secKillId.toString(), tokenQuantity);
    }

    @Async
    void sendSecKillMsg(Long secKillId, String phone) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("secKillId", secKillId);
        jsonObject.put("phone", phone);
        spikeCommodityProducer.send(jsonObject);
    }
}
