package site.clzblog.application.spike.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SecKillEntity {
    /**
     * 库存id
     */
    private Long secKillId;
    /**
     * 商品名称
     */
    private String name;

    /**
     * 库存数量
     */
    private Long inventory;

    /**
     * 开启时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 秒杀抢购版本（乐观锁）
     */
    private Long version;
}
