package site.clzblog.application.spike.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity {
    /**
     * 库存id
     */
    private Long secKillId;
    /**
     * 用户手机号码
     */
    private String userPhone;
    /**
     * 订单状态
     */
    private Long state;
    /**
     * 创建时间
     */
    private Date createTime;
}
