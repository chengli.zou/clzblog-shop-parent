package site.clzblog.application.spike.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.spike.mapper.OrderMapper;
import site.clzblog.application.spike.mapper.entity.OrderEntity;
import site.clzblog.application.spike.service.OrderSecKillService;

@RestController
public class OrderSecKillServiceImpl extends BaseApiService<JSONObject> implements OrderSecKillService {
    private final OrderMapper orderMapper;

    public OrderSecKillServiceImpl(OrderMapper orderMapper) {
        this.orderMapper = orderMapper;
    }

    @Override
    public BaseResponse<JSONObject> getOrder(String phone, Long secKillId) {
        if (StringUtils.isEmpty(phone)) return setResultError("User phone cannot be empty");

        if (null == secKillId) return setResultError("Commodity id cannot be null");

        OrderEntity orderEntity = orderMapper.selectByPhoneSecKillId(phone, secKillId);

        if (null == orderEntity) return setResultError("Being queued");

        return setResultSuccess("Congratulations spike successfully");
    }
}
