package site.clzblog.application.spike.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import site.clzblog.application.spike.mapper.entity.SecKillEntity;

public interface SecKillMapper {
    @Select("SELECT sec_kill_id as secKillId,name,inventory,start_time as startTime,end_time as endTime,create_time as createTime,version from clzblog_sec_kill where sec_kill_id=#{secKillId}")
    SecKillEntity selectBySecKillId(Long secKillId);

    @Update("update clzblog_sec_kill set inventory = inventory - 1, version = version + 1 where sec_kill_id=#{secKillId} and inventory > 0 and version=#{version}")
    int inventoryDeduction(@Param("secKillId") Long secKillId, @Param("version") Long version);
}
