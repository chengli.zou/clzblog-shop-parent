package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableEurekaClient
@EnableHystrix
@EnableApolloConfig
@MapperScan(basePackages = "site.clzblog.application.spike.mapper")
public class ApplicationSpike {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationSpike.class, args);
    }
}
