package site.clzblog.application.spike.rabbit.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.application.spike.mapper.OrderMapper;
import site.clzblog.application.spike.mapper.SecKillMapper;
import site.clzblog.application.spike.mapper.entity.OrderEntity;
import site.clzblog.application.spike.mapper.entity.SecKillEntity;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@Component
public class SpikeCommodityConsumer {
    private final SecKillMapper secKillMapper;
    private final OrderMapper orderMapper;

    public SpikeCommodityConsumer(SecKillMapper secKillMapper, OrderMapper orderMapper) {
        this.secKillMapper = secKillMapper;
        this.orderMapper = orderMapper;
    }

    @RabbitListener(queues = "modify_inventory_queue")
    @Transactional
    public void process(Message message, @Headers Map<String, Object> headers) {
        headers.forEach((key, value) -> log.debug("Rabbit mq headers key -> {}, val -> {}", key, value));
        String messageId = message.getMessageProperties().getMessageId();
        String msg = new String(message.getBody(), StandardCharsets.UTF_8);
        log.info("Rabbit mq message id -> {}, data -> {}", messageId, msg);
        JSONObject jsonObject = JSONObject.parseObject(msg);
        Long secKillId = jsonObject.getLong("secKillId");
        SecKillEntity seckillEntity = secKillMapper.selectBySecKillId(secKillId);
        if (seckillEntity == null) {
            log.warn("Commodity info don't exists {}", secKillId);
            return;
        }
        Long version = seckillEntity.getVersion();
        int inventoryDeduction = secKillMapper.inventoryDeduction(secKillId, version);
        if (inventoryDeduction < 0) {
            log.info("Spike id -> {} result -> {} failure", secKillId, inventoryDeduction);
            return;
        }
        OrderEntity orderEntity = new OrderEntity();
        String phone = jsonObject.getString("phone");
        orderEntity.setUserPhone(phone);
        orderEntity.setSecKillId(secKillId);
        orderEntity.setState(1L);
        int insertOrder = orderMapper.insertOrder(orderEntity);
        if (insertOrder < 0) return;
        log.info("Spike id -> {} result -> {} success", secKillId, inventoryDeduction);
    }
}
