package site.clzblog.application.spike.rabbit.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConfig {

    public static final String MODIFY_INVENTORY_QUEUE = "modify_inventory_queue";
    public static final String MODIFY_EXCHANGE_NAME = "modify_exchange_name";
    public static final String MODIFY_ROUTING_KEY = "modifyRoutingKey";

    @Bean
    public Queue directModifyInventoryQueue() {
        return new Queue(MODIFY_INVENTORY_QUEUE);
    }

    @Bean
    DirectExchange directModifyExchange() {
        return new DirectExchange(MODIFY_EXCHANGE_NAME);
    }

    @Bean
    Binding bindingModifyRoutingKey() {
        return BindingBuilder.bind(directModifyInventoryQueue()).to(directModifyExchange()).with(MODIFY_ROUTING_KEY);
    }
}
