package site.clzblog.application.spike.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import site.clzblog.application.spike.mapper.entity.OrderEntity;

public interface OrderMapper {
    @Insert("INSERT INTO clzblog_order VALUES (#{secKillId},#{userPhone}, '1', now())")
    int insertOrder(OrderEntity orderEntity);

    @Select("SELECT sec_kill_id AS secKillId,user_phone as userPhone , state as state FROM clzblog_order WHERE user_phone=#{phone}  and sec_kill_id=#{secKillId}  AND state='1'")
    OrderEntity selectByPhoneSecKillId(@Param("phone") String phone, @Param("secKillId") Long secKillId);
}
