package site.clzblog.application.spike.rabbit.mq.producer;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.application.spike.rabbit.mq.config.RabbitMQConfig;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
@Component
public class SpikeCommodityProducer implements RabbitTemplate.ConfirmCallback {
    private final RabbitTemplate rabbitTemplate;

    public SpikeCommodityProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Transactional
    public void send(JSONObject jsonObject) {
        String jsonString = jsonObject.toJSONString();
        String messAgeId = UUID.randomUUID().toString().replace("-", "");
        Message message = MessageBuilder.withBody(jsonString.getBytes())
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .setContentEncoding(StandardCharsets.UTF_8.toString())
                .setMessageId(messAgeId)
                .build();
        this.rabbitTemplate.setMandatory(true);
        this.rabbitTemplate.setConfirmCallback(this);
        CorrelationData correlationData = new CorrelationData(jsonString);
        rabbitTemplate.convertAndSend(RabbitMQConfig.MODIFY_EXCHANGE_NAME, RabbitMQConfig.MODIFY_ROUTING_KEY, message, correlationData);
    }


    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String s) {
        log.info("Rabbit mq message id -> {}", correlationData.getId());
        if (ack) log.info("Rabbit mq message acknowledged");
        else send(JSONObject.parseObject(s));
    }
}
