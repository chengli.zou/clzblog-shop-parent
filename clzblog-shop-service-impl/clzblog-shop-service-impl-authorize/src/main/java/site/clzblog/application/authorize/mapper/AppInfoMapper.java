package site.clzblog.application.authorize.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import site.clzblog.application.authorize.mapper.entity.AppInfo;

public interface AppInfoMapper {
    @Insert("INSERT INTO clzblog_app_info VALUES (null, #{appId},#{appName}, #{appSecret}, '0', null, null, null, null, null)")
    int insert(AppInfo appInfo);

    @Select("SELECT id AS id ,app_id as appId, app_name AS appName ,app_secret as appSecret FROM clzblog_app_info where app_id=#{appId} and app_secret=#{appSecret}")
    AppInfo selectByAppIdSecret(@Param("appId") String appId, @Param("appSecret") String appSecret);

    @Select("SELECT id AS id ,app_id as appId, app_name AS appName ,app_secret as appSecret FROM clzblog_app_info where app_id=#{appId}")
    AppInfo selectByAppId(@Param("appId") String appId);

    @Select("SELECT id AS id ,app_id as appId, app_name AS appName ,app_secret as appSecret FROM clzblog_app_info where app_name=#{appName}")
    AppInfo selectByAppName(@Param("appName") String appName);
}
