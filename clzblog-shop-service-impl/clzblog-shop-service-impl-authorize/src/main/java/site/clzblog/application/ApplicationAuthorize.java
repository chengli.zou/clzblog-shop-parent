package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@MapperScan(basePackages = "site.clzblog.application.authorize.mapper")
@EnableApolloConfig
@EnableEurekaClient
@SpringBootApplication
public class ApplicationAuthorize {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationAuthorize.class, args);
    }
}
