package site.clzblog.application.authorize.guid;

import site.clzblog.application.common.core.utils.MD5Util;

import java.util.Objects;
import java.util.UUID;

public class Guid {
    private String appKey;

    private String guid() {
        return UUID.randomUUID().toString();
    }

    public String getAppId() {
        Guid guid = new Guid();
        appKey = guid.guid();
        return appKey;
    }

    public String getAppSecret() {
        return Objects.requireNonNull(MD5Util.MD5(String.format("key%s", appKey))).toUpperCase();
    }

    public static void main(String[] args) {
        Guid guid = new Guid();
        System.out.println(guid.getAppId());

        System.out.println(guid.getAppSecret());
    }
}
