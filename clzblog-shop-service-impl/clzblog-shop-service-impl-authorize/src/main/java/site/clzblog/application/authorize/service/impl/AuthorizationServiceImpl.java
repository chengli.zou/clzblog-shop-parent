package site.clzblog.application.authorize.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.authorize.guid.Guid;
import site.clzblog.application.authorize.mapper.AppInfoMapper;
import site.clzblog.application.authorize.mapper.entity.AppInfo;
import site.clzblog.application.authorize.service.AuthorizationService;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.RedisUtils;
import site.clzblog.application.common.token.GenerateToken;

@RestController
public class AuthorizationServiceImpl extends BaseApiService<JSONObject> implements AuthorizationService {
    private final AppInfoMapper appInfoMapper;

    private final GenerateToken generateToken;

    private final RedisUtils redisUtils;

    public AuthorizationServiceImpl(AppInfoMapper appInfoMapper, GenerateToken generateToken, RedisUtils redisUtils) {
        this.appInfoMapper = appInfoMapper;
        this.generateToken = generateToken;
        this.redisUtils = redisUtils;
    }

    @Override
    public BaseResponse<JSONObject> applyAppInfo(String appName) {
        if (StringUtils.isEmpty(appName)) return setResultError("App name cannot be empty");

        AppInfo info = appInfoMapper.selectByAppName(appName);
        if (info != null) return returnAppIdSecret(info.getAppId(), info.getAppSecret());

        Guid guid = new Guid();
        String appId = guid.getAppId();
        String appSecret = guid.getAppSecret();

        AppInfo appInfo = new AppInfo(appName, appId, appSecret);
        int insert = appInfoMapper.insert(appInfo);
        if (!toDaoResult(insert)) return setResultError("Apply app info failure");

        return returnAppIdSecret(appId, appSecret);
    }

    private BaseResponse<JSONObject> returnAppIdSecret(String appId, String appSecret) {
        JSONObject data = new JSONObject();
        data.put("appId", appId);
        data.put("appSecret", appSecret);

        return setResultSuccess(data);
    }

    @Override
    public BaseResponse<JSONObject> getAccessToken(String appId, String appSecret) {
        if (StringUtils.isEmpty(appId)) return setResultError("App id cannot be empty");

        if (StringUtils.isEmpty(appSecret)) return setResultError("App secret cannot be empty");

        AppInfo appInfo = appInfoMapper.selectByAppIdSecret(appId, appSecret);
        if (null == appInfo) return setResultError("App id or app secret error");

        String key = String.format("%s-%s", appId, appSecret);
        String oldToken = redisUtils.getString(key);

        if (null != oldToken) return returnAccessToken(oldToken);

        String token = generateToken.createToken("authorize", appInfo.getAppId());
        redisUtils.setString(key, token);

        return returnAccessToken(token);
    }

    private BaseResponse<JSONObject> returnAccessToken(String token) {
        JSONObject data = new JSONObject();
        data.put("accessToken", token);
        return setResultSuccess(data);
    }

    @Override
    public BaseResponse<JSONObject> getAppInfo(String accessToken) {
        if (StringUtils.isEmpty(accessToken)) return setResultError("Access token cannot be empty");

        String appId = generateToken.getToken(accessToken);
        if (StringUtils.isEmpty(appId)) return setResultError("Access token invalid");

        AppInfo appInfo = appInfoMapper.selectByAppId(appId);
        if (null == appInfo) return setResultError("Access token invalid");

        JSONObject data = new JSONObject();
        data.put("appInfo", appInfo);

        return setResultSuccess(data);
    }
}
