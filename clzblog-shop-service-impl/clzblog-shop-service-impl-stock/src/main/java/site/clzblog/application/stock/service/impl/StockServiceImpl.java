package site.clzblog.application.stock.service.impl;

import com.codingapi.tx.annotation.TxTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.stock.entity.StockEntity;
import site.clzblog.application.stock.mapper.StockMapper;
import site.clzblog.application.stock.service.StockService;

@RestController
public class StockServiceImpl extends BaseApiService implements StockService {
    private final StockMapper stockMapper;

    @Autowired
    public StockServiceImpl(StockMapper stockMapper) {
        this.stockMapper = stockMapper;
    }

    @TxTransaction()
    @Transactional
    @GetMapping("inventory-reduction")
    @Override
    public BaseResponse inventoryReduction(Long commodityId) {
        if (null == commodityId) return setResultError("Commodity id cannot be null");
        StockEntity stockEntity = stockMapper.selectStock(commodityId);

        if (null == stockEntity) return setResultError("Commodity id don't exists");

        if (stockEntity.getStock() <= 0) return setResultError("Commodity sold out");

        if (stockMapper.updateStock(commodityId) <= 0) return setResultError("Modify stock failure");

        return setResultSuccess("Modify stock successfully");
    }
}
