package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@EnableApolloConfig
@SpringBootApplication
@MapperScan(basePackages = "site.clzblog.application.stock.mapper")
public class ApplicationStock {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationStock.class, args);
    }
}
