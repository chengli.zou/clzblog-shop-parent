package site.clzblog.application.stock.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import site.clzblog.application.stock.entity.StockEntity;

public interface StockMapper {
    @Select("SELECT id as id ,commodity_id as  commodityId, stock as stock from stock where commodity_id=#{commodityId}")
    StockEntity selectStock(@Param("commodityId") Long commodityId);

    @Update("update stock set stock=stock-1 where commodity_id=#{commodityId}")
    int updateStock(@Param("commodityId") Long commodityId);
}
