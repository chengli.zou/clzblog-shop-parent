package site.clzblog.application.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.stock.service.StockService;

@FeignClient("app-clzblog-stock")
public interface StockServiceFeign extends StockService {
}
