package site.clzblog.application.order.service.impl;

import com.codingapi.tx.annotation.TxTransaction;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.order.entity.OrderEntity;
import site.clzblog.application.order.feign.MemberServiceFeign;
import site.clzblog.application.order.feign.StockServiceFeign;
import site.clzblog.application.order.mapper.OrderMapper;
import site.clzblog.application.order.service.OrderService;

import java.util.Date;

@RestController
public class OrderServiceImpl extends BaseApiService implements OrderService {
    private final StockServiceFeign stockServiceFeign;

    private final MemberServiceFeign memberServiceFeign;

    private final OrderMapper orderMapper;

    @Autowired
    public OrderServiceImpl(StockServiceFeign stockServiceFeign, MemberServiceFeign memberServiceFeign, OrderMapper orderMapper) {
        this.stockServiceFeign = stockServiceFeign;
        this.memberServiceFeign = memberServiceFeign;
        this.orderMapper = orderMapper;
    }

    @TxTransaction(isStart = true)
    @GetMapping("add-order-and-stock")
    @Transactional
    @Override
    public BaseResponse addOrderAndStock(int i) throws Exception {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setName("VIP");
        orderEntity.setOrderCreateTime(new Date());
        orderEntity.setOrderMoney(300d);
        orderEntity.setOrderState(0);
        Long commodityId = 30L;
        orderEntity.setCommodityId(commodityId);
        int order = orderMapper.addOrder(orderEntity);
        if (order <= 0) return setResultError("Order failure");
        BaseResponse response = stockServiceFeign.inventoryReduction(commodityId);
        if (!Constants.HTTP_RES_CODE_200.equals(response.getCode())) throw new Exception("Call stock interface error");
        int result = 1 / i;
        System.out.println("result:" + result);
        return setResultSuccess("Order successfully");
    }

    @Override
    @HystrixCommand(fallbackMethod = "orderToMemberTestGetUserInfoFallBack")
    @GetMapping("order-to-member-test-get-user-info")
    public BaseResponse orderToMemberTestGetUserInfo() {
        System.out.println("Current thread name:" + Thread.currentThread().getName());
        return memberServiceFeign.testGetUserInfo();
    }

    public BaseResponse orderToMemberTestGetUserInfoFallBack() {
        System.out.println("orderToMemberTestGetUserInfoFallBack");
        return setResultSuccess("Server are too busy,please try again later.");
    }
}
