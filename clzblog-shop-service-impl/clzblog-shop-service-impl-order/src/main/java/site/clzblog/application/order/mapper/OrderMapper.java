package site.clzblog.application.order.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import site.clzblog.application.order.entity.OrderEntity;

public interface OrderMapper {
    @Insert(value = "INSERT INTO `order` VALUES (#{id}, #{name}, #{orderCreateTime}, #{orderMoney}, #{orderState}, #{commodityId})")
    @Options(useGeneratedKeys = true, keyColumn = "id")
    int addOrder(OrderEntity orderEntity);
}
