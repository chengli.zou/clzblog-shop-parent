package site.clzblog.application.member.mapper.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTokenDO extends BaseDO {
    private Long id;
    private String token;
    private String loginType;
    private String deviceInfo;
    private Long isAvailable;
    private Long userId;
    private Date createDate;
    private Date updateDate;
}
