package site.clzblog.application.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.common.core.utils.MD5Util;
import site.clzblog.application.member.feigh.VerifyCodeServiceFeign;
import site.clzblog.application.member.input.dto.UserInputDTO;
import site.clzblog.application.member.mapper.UserMapper;
import site.clzblog.application.member.mapper.domain.UserDO;
import site.clzblog.application.member.service.MemberRegisterService;

@RestController
public class MemberRegisterServiceImpl extends BaseApiService<JSONObject> implements MemberRegisterService {
    private final UserMapper userMapper;

    private final VerifyCodeServiceFeign verifyCodeServiceFeign;

    @Autowired
    public MemberRegisterServiceImpl(UserMapper userMapper, VerifyCodeServiceFeign verifyCodeServiceFeign) {
        this.userMapper = userMapper;
        this.verifyCodeServiceFeign = verifyCodeServiceFeign;
    }

    @Transactional
    @Override
    public BaseResponse<JSONObject> register(@RequestBody UserInputDTO userEntity, String registerCode) {
        //if (StringUtils.isEmpty(userEntity.getUsername())) return setResultError("User name cannot be empty.");
        String mobile = userEntity.getMobile();
        if (StringUtils.isEmpty(mobile)) return setResultError("Mobile phone number cannot be empty.");
        if (null != userMapper.existMobile(mobile)) return setResultError("Mobile phone number existed.");
        //String email = userEntity.getEmail();
        //if (StringUtils.isEmpty(email)) return setResultError("Email cannot be empty.");
        //if (null != userMapper.existEmail(email)) return setResultError("Email existed");
        BaseResponse<JSONObject> verify = verifyCodeServiceFeign.verifyWechatCode(userEntity.getMobile(), registerCode);
        if (!verify.getCode().equals(Constants.HTTP_RES_CODE_200)) return setResultError(verify.getMsg());
        String password = userEntity.getPassword();
        if (StringUtils.isEmpty(password)) return setResultError("Password cannot be empty.");
        userEntity.setPassword(MD5Util.MD5(password));
        return userMapper.register(BeanUtils.dtoToDo(userEntity, UserDO.class)) == 1 ? setResultSuccess("Register successfully") : setResultError("Register failure");
    }
}
