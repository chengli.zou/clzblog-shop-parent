package site.clzblog.application.member.feigh;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.wechat.service.VerifyCodeService;

@FeignClient("app-clzblog-wechat")
public interface VerifyCodeServiceFeign extends VerifyCodeService {
}
