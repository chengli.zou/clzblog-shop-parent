package site.clzblog.application.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.member.mapper.UserMapper;
import site.clzblog.application.member.mapper.domain.UserDO;
import site.clzblog.application.member.service.QQAuthService;

@RestController
public class QQAuthServiceImpl extends BaseApiService<JSONObject> implements QQAuthService {
    private final UserMapper userMapper;

    private final GenerateToken generateToken;

    @Autowired
    public QQAuthServiceImpl(UserMapper userMapper, GenerateToken generateToken) {
        this.userMapper = userMapper;
        this.generateToken = generateToken;
    }

    @Override
    @RequestMapping("find/by/{open-id}")
    public BaseResponse<JSONObject> findByOpenId(@PathVariable("open-id") String openId) {
        if (StringUtils.isEmpty(openId)) return setResultError("QQ open id cannot be empty");
        UserDO userDO = userMapper.findByQQOpenId(openId);
        if (null == userDO) return setResultError(Constants.HTTP_RES_CODE_203, "Not found open id");
        JSONObject data = new JSONObject();
        data.put("token", generateToken.createToken(Constants.MEMBER_TOKEN_KEY_PREFIX_QQ, userDO.getUserId().toString()));
        return setResultSuccess(data);
    }
}
