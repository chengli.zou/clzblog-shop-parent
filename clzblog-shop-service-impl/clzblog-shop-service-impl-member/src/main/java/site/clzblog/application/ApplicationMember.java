package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableSwagger2Doc
@EnableHystrix
@EnableApolloConfig
@MapperScan(basePackages = "site.clzblog.application.member.mapper")
public class ApplicationMember {
    @Value("${spring.application.name}")
    private String name;

    @GetMapping("info")
    public String info() {
        return name;
    }

    public static void main(String[] args) {
        SpringApplication.run(ApplicationMember.class, args);
    }
}
