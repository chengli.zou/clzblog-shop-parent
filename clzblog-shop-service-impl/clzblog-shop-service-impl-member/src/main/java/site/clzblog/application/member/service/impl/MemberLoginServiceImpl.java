package site.clzblog.application.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.core.utils.MD5Util;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.common.transaction.RedisDataSourceTransaction;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;
import site.clzblog.application.member.mapper.UserMapper;
import site.clzblog.application.member.mapper.UserTokenMapper;
import site.clzblog.application.member.mapper.domain.UserDO;
import site.clzblog.application.member.mapper.domain.UserTokenDO;
import site.clzblog.application.member.service.MemberLoginService;

@RestController
public class MemberLoginServiceImpl extends BaseApiService<JSONObject> implements MemberLoginService {
    private final UserMapper userMapper;

    private final UserTokenMapper userTokenMapper;

    private final GenerateToken generateToken;

    private final RedisDataSourceTransaction manualTransaction;

    @Autowired
    public MemberLoginServiceImpl(UserMapper userMapper, UserTokenMapper userTokenMapper, GenerateToken generateToken, RedisDataSourceTransaction manualTransaction) {
        this.userMapper = userMapper;
        this.userTokenMapper = userTokenMapper;
        this.generateToken = generateToken;
        this.manualTransaction = manualTransaction;
    }

    @Override
    public BaseResponse<JSONObject> login(@RequestBody UserLoginInputDTO userLoginInputDTO) {
        String mobile = userLoginInputDTO.getMobile();
        if (StringUtils.isEmpty(mobile)) return setResultError("Mobile phone number cannot be empty.");
        String password = userLoginInputDTO.getPassword();
        if (StringUtils.isEmpty(password)) return setResultError("Password cannot be empty.");
        String loginType = userLoginInputDTO.getLoginType();
        if (StringUtils.isEmpty(loginType)) return setResultError("Login type cannot be empty.");
        if (!(Constants.MEMBER_LOGIN_TYPE_ANDROID.equalsIgnoreCase(loginType)
                || Constants.MEMBER_LOGIN_TYPE_IOS.equalsIgnoreCase(loginType)
                || Constants.MEMBER_LOGIN_TYPE_PC.equalsIgnoreCase(loginType))) {
            return setResultError("Login type appear error.");
        }
        String deviceInfo = userLoginInputDTO.getDeviceInfo();
        if (StringUtils.isEmpty(deviceInfo)) return setResultError("Device information cannot be empty.");
        UserDO login = userMapper.login(mobile, MD5Util.MD5(password));
        if (login == null) return setResultError("Mobile phone number or password error.");
        TransactionStatus status = null;
        try {
            status = manualTransaction.begin();
            Long userId = login.getUserId();
            UserTokenDO userTokenDO = userTokenMapper.selectByUserIdAndLoginType(userId, loginType);
            if (null != userTokenDO) {
                generateToken.removeToken(userTokenDO.getToken());
                if (userTokenMapper.updateTokenAvailable(userId, loginType) < 0) {
                    manualTransaction.rollback(status);
                    setResultError("System error!");
                }
            }
            String token = generateToken.createToken(Constants.MEMBER_TOKEN_KEY_PREFIX, userId.toString(), Constants.TOKEN_MEMBER_TIME);
            int i = userTokenMapper.insertUserToken(new UserTokenDO(null, token, loginType, deviceInfo, null, userId, null, null));
            if (!toDaoResult(i)) {
                manualTransaction.rollback(status);
                setResultError("System error!");
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("token", token);
            manualTransaction.commit(status);
            return setResultSuccess(jsonObject);
        } catch (Exception e) {
            manualTransaction.rollback(status);
            return setResultError(String.format("System error!%s", e.toString()));
        }

    }

    @Override
    @DeleteMapping("del/{token}")
    public BaseResponse<JSONObject> delToken(@PathVariable("token") String token) {
        if (!StringUtils.isEmpty(token)) setResultError("Token cannot be empty!");
        try {
            int i = userTokenMapper.deleteByToken(token);
            if (!toDaoResult(i)) return setResultError("Token don't exists!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = new JSONObject();
        object.put("token", token);
        return setResultSuccess("Token deleted!", object);
    }

}
