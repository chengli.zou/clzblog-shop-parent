package site.clzblog.application.member.mapper;

import org.apache.ibatis.annotations.*;
import site.clzblog.application.member.mapper.domain.UserTokenDO;

public interface UserTokenMapper {
    @Select("SELECT id,token,login_type as LoginType, device_info as deviceInfo ,is_available as isAvailable,user_id as userId,create_time as createTime,update_time as updateTime FROM t_user_login WHERE user_id=#{userId} AND login_type=#{loginType} and is_available ='0'")
    UserTokenDO selectByUserIdAndLoginType(@Param("userId") Long userId, @Param("loginType") String loginType);

    @Update("update t_user_login set is_available ='1',update_time=now() where user_id=#{userId} and login_type =#{loginType}")
    int updateTokenAvailable(@Param("userId") Long userId, @Param("loginType") String loginType);

    @Insert("INSERT INTO t_user_login VALUES (null,#{token},#{loginType},#{deviceInfo},0,#{userId},now(),null)")
    int insertUserToken(UserTokenDO userTokenDo);

    @Delete("delete from t_user_login where token=#{token}")
    int deleteByToken(@Param("token") String token);
}
