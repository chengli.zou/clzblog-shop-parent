package site.clzblog.application.member.mapper.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDO {
    private Long userId;
    private String mobile;
    private String email;
    private String password;
    private String username;
    private char gender;
    private int age;
    private Date createTime;
    private Date updateTime;
    private char isAvailable;
    private String userAvatar;
    private String qqOpenId;
    private String wechatOpenId;
}
