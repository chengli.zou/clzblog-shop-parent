package site.clzblog.application.member.feigh;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.wechat.service.WeChatAppService;

@FeignClient(name = "app-clzblog-wechat")
public interface WeChatAppServiceFeign extends WeChatAppService {
}
