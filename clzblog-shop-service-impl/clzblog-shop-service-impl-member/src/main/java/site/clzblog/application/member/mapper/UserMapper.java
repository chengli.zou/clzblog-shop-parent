package site.clzblog.application.member.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import site.clzblog.application.member.mapper.domain.UserDO;

public interface UserMapper {
    @Insert("insert into t_user values(null,#{mobile}, #{email}, #{password}, #{username}, null, null, null, null,'1', null, null, null)")
    int register(UserDO userEntity);

    @Select("select user_id userId,mobile,email,password,username,gender,age,create_time createTime,update_time updateTime,is_available isAvailable,user_avatar userAvatar,qq_open_id qqOpenId,wechat_open_id wechatOpenId from t_user where mobile=#{mobile}")
    UserDO existMobile(@Param("mobile") String mobile);

    @Select("select user_id userId,mobile,email,password,username,gender,age,create_time createTime,update_time updateTime,is_available isAvailable,user_avatar userAvatar,qq_open_id qqOpenId,wechat_open_id wechatOpenId from t_user where email=#{email}")
    UserDO existEmail(@Param("email") String email);

    @Select("SELECT user_id userId,mobile,email,password,username,gender,age,create_time createTime,update_time updateTime,is_available isAvailable,user_avatar userAvatar,qq_open_id qqOpenId,wechat_open_id wechatOpenId FROM t_user WHERE MOBILE=#{mobile} and password=#{password};")
    UserDO login(@Param("mobile") String mobile, @Param("password") String password);

    @Select("SELECT user_id as userId,mobile,email,password,username,gender,age,create_time as createTime,update_time as updateTime,is_available as isAvailable,user_avatar as userAvatar,qq_open_id as qqOpenId,wechat_open_id as wechatOpenId from t_user WHERE user_id=#{userId}")
    UserDO findByUserId(@Param("userId") Long userId);

    @Select("SELECT user_id as userId,mobile,email,password,username,gender,age,create_time as createTime,update_time as updateTime,is_available as isAvailable,user_avatar as userAvatar,qq_open_id as qqOpenId,wechat_open_id as wechatOpenId from t_user WHERE qq_open_id=#{openId}")
    UserDO findByQQOpenId(@Param("openId") String openId);
}
