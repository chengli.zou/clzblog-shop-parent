package site.clzblog.application.member.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.common.core.utils.MD5Util;
import site.clzblog.application.common.helper.TypeCastHelper;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;
import site.clzblog.application.member.mapper.UserMapper;
import site.clzblog.application.member.mapper.domain.UserDO;
import site.clzblog.application.member.output.dto.UserOutputDTO;
import site.clzblog.application.member.service.MemberAppService;

@RestController
public class MemberAppServiceImpl extends BaseApiService<UserOutputDTO> implements MemberAppService {
    @Value("${server.port}")
    private String port;

    private final UserMapper userMapper;
    private final GenerateToken generateToken;

    @Autowired
    public MemberAppServiceImpl(UserMapper userMapper, GenerateToken generateToken) {
        this.userMapper = userMapper;
        this.generateToken = generateToken;
    }

    @Override
    public BaseResponse<UserOutputDTO> existMobile(String mobile) {
        if (StringUtils.isEmpty(mobile)) return setResultError("Mobile phone number cannot be empty.");
        UserDO userEntity = userMapper.existMobile(mobile);
        if (userEntity == null) return setResultError(Constants.HTTP_RES_CODE_203, "User information don't exist.");
        userEntity.setPassword(null);
        return setResultSuccess(BeanUtils.doToDto(userEntity, UserOutputDTO.class));
    }

    @Override
    public BaseResponse<UserOutputDTO> getUserInfo(String token) {
        if (StringUtils.isEmpty(token)) return setResultError("Token cannot be empty.");

        String redisToken = generateToken.getToken(token);

        if (StringUtils.isEmpty(redisToken)) return setResultError("Token incorrect or expire.");

        UserDO userDO = userMapper.findByUserId(TypeCastHelper.toLong(redisToken));

        if (null == userDO) return setResultError("User information don't exists");

        return setResultSuccess(BeanUtils.doToDto(userDO, UserOutputDTO.class));
    }

    @Override
    public BaseResponse<UserOutputDTO> ssoLogin(@RequestBody UserLoginInputDTO userLoginInputDTO) {
        String mobile = userLoginInputDTO.getMobile();
        if (StringUtils.isEmpty(mobile)) return setResultError("Mobile phone number cannot be empty!");

        String password = userLoginInputDTO.getPassword();
        if (StringUtils.isEmpty(password)) return setResultError("Password cannot be empty!");

        String loginType = userLoginInputDTO.getLoginType();
        if (StringUtils.isEmpty(loginType)) return setResultError("Login type cannot be empty!");
        if (!(loginType.equals(Constants.MEMBER_LOGIN_TYPE_ANDROID)
                || loginType.equals(Constants.MEMBER_LOGIN_TYPE_IOS)
                || loginType.equals(Constants.MEMBER_LOGIN_TYPE_PC))) return setResultError("Login type occur error!");

        String deviceInfo = userLoginInputDTO.getDeviceInfo();
        if (StringUtils.isEmpty(deviceInfo)) return setResultError("Device information cannot be empty!");

        String newPassWord = MD5Util.MD5(password);
        UserDO userDo = userMapper.login(mobile, newPassWord);
        if (userDo == null) return setResultError("Username or password error!");

        return setResultSuccess(BeanUtils.doToDto(userDo, UserOutputDTO.class));
    }

    @Override
    public BaseResponse testGetUserInfo() {
       /* try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return setResultSuccess("Test get user info successfully" + port);
    }

    @HystrixCommand(fallbackMethod = "testHystrixFallback")
    @GetMapping("test-hystrix")
    public BaseResponse testHystrix() {
        System.out.println("Current thread name:" + Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return setResultSuccess("Success");
    }

    public BaseResponse testHystrixFallback() {
        System.out.println("Current thread name:" + Thread.currentThread().getName());
        return setResultSuccess("Fall back server are too busy please try again later");
    }

}
