package site.clzblog.application.member.mapper.domain;

import lombok.Data;

import java.util.Date;

@Data
class BaseDO {
    private Date createTime;
    private Date updateTime;
    private Long id;
    private Long isAvailable;
}
