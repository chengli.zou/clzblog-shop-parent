package site.clzblog.application.wechat.service.impl;

import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.wechat.entity.AppEntity;
import site.clzblog.application.wechat.service.WeChatAppService;

@RestController
public class WeChatAppServiceImpl extends BaseApiService<AppEntity> implements WeChatAppService {
    @Override
    public BaseResponse<AppEntity> getApp() {
        return setResultSuccess(new AppEntity("58790713","chengli.zou"));
    }
}
