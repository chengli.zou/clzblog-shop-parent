package site.clzblog.application.wechat.mp.handler;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.core.utils.RedisUtils;
import site.clzblog.application.common.core.utils.RegexUtils;
import site.clzblog.application.member.output.dto.UserOutputDTO;
import site.clzblog.application.wechat.feign.MemberServiceFeign;
import site.clzblog.application.wechat.mp.builder.TextBuilder;

import java.util.Map;

import static me.chanjar.weixin.common.api.WxConsts.XmlMsgType;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class MsgHandler extends AbstractHandler {
    private final RedisUtils redisUtils;

    @Value("${clzblog.wechat.registration.code.message}")
    private String codeMessage;

    @Value("${clzblog.wechat.registration.default.code.message}")
    private String defaultCodeMessage;

    private final MemberServiceFeign memberServiceFeign;

    @Autowired
    public MsgHandler(RedisUtils redisUtils, MemberServiceFeign memberServiceFeign) {
        this.redisUtils = redisUtils;
        this.memberServiceFeign = memberServiceFeign;
    }

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) {

        if (!wxMessage.getMsgType().equals(XmlMsgType.EVENT)) {
            //TODO 可以选择将消息保存到本地
        }

        //当用户输入关键词如“你好”，“客服”等，并且有客服在线时，把消息转发给在线客服
        try {
            if (StringUtils.startsWithAny(wxMessage.getContent(), "你好", "客服")
                    && weixinService.getKefuService().kfOnlineList()
                    .getKfOnlineList().size() > 0) {
                return WxMpXmlOutMessage.TRANSFER_CUSTOMER_SERVICE()
                        .fromUser(wxMessage.getToUser())
                        .toUser(wxMessage.getFromUser()).build();
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

        //TODO 组装回复消息
        String content = wxMessage.getContent();

        if (RegexUtils.checkPhone(content)) {
            BaseResponse<UserOutputDTO> existMobile = memberServiceFeign.existMobile(content);
            if (existMobile.getCode().equals(Constants.HTTP_RES_CODE_200)) {
                return new TextBuilder().build("The mobile phone number existed", wxMessage, weixinService);
            }
            if (!existMobile.getCode().equals(Constants.HTTP_RES_CODE_203)) {
                return new TextBuilder().build(existMobile.getMsg(), wxMessage, weixinService);
            }
            int code = getRegisterCode();
            String registerCode = String.format(codeMessage, code);
            redisUtils.setString(String.format(Constants.WEIXINCODE_KEY, content), code + "", Constants.WEIXINCODE_TIMEOUT);
            return new TextBuilder().build(registerCode, wxMessage, weixinService);
        }

        return new TextBuilder().build(defaultCodeMessage, wxMessage, weixinService);

    }

    // 获取注册码
    private int getRegisterCode() {
        int registerCode = (int) (Math.random() * 9000 + 1000);
        return registerCode;
    }

}
