package site.clzblog.application.wechat.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.member.service.MemberAppService;

@FeignClient("app-clzblog-member")
public interface MemberServiceFeign extends MemberAppService {
}
