package site.clzblog.application.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.core.utils.RedisUtils;
import site.clzblog.application.wechat.service.VerifyCodeService;

@RestController
public class VerifyCodeServiceImpl extends BaseApiService<JSONObject> implements VerifyCodeService {
    private final RedisUtils redisUtils;

    @Autowired
    public VerifyCodeServiceImpl(RedisUtils redisUtils) {
        this.redisUtils = redisUtils;
    }

    @Override
    public BaseResponse<JSONObject> verifyWechatCode(String phone, String wechatCode) {
        if (StringUtils.isEmpty(phone)) return setResultError("Phone number cannot be empty.");
        if (StringUtils.isEmpty(wechatCode)) return setResultError("Wechat code cannot be empty.");
        String redisCode = redisUtils.getString(String.format(Constants.WEIXINCODE_KEY, phone));
        if (StringUtils.isEmpty(redisCode)) return setResultError("Wechat code expired.Please you resend.");
        if (!redisCode.equals(wechatCode)) return setResultError("Wechat code incorrect.");
        return setResultSuccess("Wechat code correct.");
    }
}
