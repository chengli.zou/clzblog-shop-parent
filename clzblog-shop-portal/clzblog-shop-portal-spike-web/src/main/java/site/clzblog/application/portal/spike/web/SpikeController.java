package site.clzblog.application.portal.spike.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpikeController {
    @RequestMapping("details/{id}")
    public String details(@PathVariable("id") Long id) {
        return "details";
    }
}
