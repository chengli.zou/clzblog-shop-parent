package site.clzblog.application.portal.pay.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.pay.service.PaymentChannelService;

@FeignClient("app-clzblog-pay")
public interface PaymentChannelFeign extends PaymentChannelService {
}
