package site.clzblog.application.portal.pay.web.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.pay.output.dto.PymtTransOutDTO;
import site.clzblog.application.portal.pay.web.feign.PayContextFeign;
import site.clzblog.application.portal.pay.web.feign.PaymentChannelFeign;
import site.clzblog.application.portal.pay.web.feign.PaymentTransInfoFeign;
import site.clzblog.application.web.base.BaseWebController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Controller
public class PayController extends BaseWebController {

    private final PayContextFeign payContextFeign;

    private final PaymentChannelFeign paymentChannelFeign;

    private final PaymentTransInfoFeign paymentTransInfoFeign;

    @Autowired
    public PayController(PayContextFeign payContextFeign,PaymentChannelFeign paymentChannelFeign, PaymentTransInfoFeign paymentTransInfoFeign) {
        this.paymentChannelFeign = paymentChannelFeign;
        this.paymentTransInfoFeign = paymentTransInfoFeign;
        this.payContextFeign = payContextFeign;
    }

    @RequestMapping("pay")
    public String pay(String payToken, Model model) {

        if (StringUtils.isEmpty(payToken)) {
            setErrorMsg(model, "Pay token cannot be empty");
            return ERROR_500_FTL;
        }

        BaseResponse<PymtTransOutDTO> paymentTrans = paymentTransInfoFeign.tokenByPaymentTrans(payToken);
        if (!isSuccess(paymentTrans)) {
            setErrorMsg(model, paymentTrans.getMsg());
            return ERROR_500_FTL;
        }

        model.addAttribute("data", paymentTrans.getData());
        model.addAttribute("paymentChannelList", paymentChannelFeign.selectAll());
        model.addAttribute("payToken", payToken);

        return "index";
    }

    @RequestMapping("pay/html")
    public void payHtml(String channelId, String payToken, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        BaseResponse<JSONObject> payHtmlData = payContextFeign.toPayHtml(channelId, payToken);
        if (isSuccess(payHtmlData)) {
            JSONObject data = payHtmlData.getData();
            String payHtml = data.getString("payHtml");
            response.getWriter().print(payHtml);
        }
    }
}
