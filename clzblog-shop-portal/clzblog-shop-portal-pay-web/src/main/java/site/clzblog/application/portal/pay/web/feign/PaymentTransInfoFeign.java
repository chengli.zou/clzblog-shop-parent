package site.clzblog.application.portal.pay.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.pay.service.PaymentTransInfoService;

@FeignClient("app-clzblog-pay")
public interface PaymentTransInfoFeign extends PaymentTransInfoService {
}
