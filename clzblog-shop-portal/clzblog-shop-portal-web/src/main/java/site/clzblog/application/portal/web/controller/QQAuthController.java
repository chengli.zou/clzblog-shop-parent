package site.clzblog.application.portal.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.oauth.Oauth;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;
import site.clzblog.application.portal.web.controller.req.vo.LoginVO;
import site.clzblog.application.portal.web.feign.MemberLoginServiceFeign;
import site.clzblog.application.portal.web.feign.QQAuthFeign;
import site.clzblog.application.web.base.BaseWebController;
import site.clzblog.application.web.constants.Constants;
import site.clzblog.application.web.utils.CookieUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class QQAuthController extends BaseWebController {
    private final QQAuthFeign qqAuthFeign;
    private final MemberLoginServiceFeign memberLoginServiceFeign;

    @Autowired
    public QQAuthController(QQAuthFeign qqAuthFeign, MemberLoginServiceFeign memberLoginServiceFeign) {
        this.qqAuthFeign = qqAuthFeign;
        this.memberLoginServiceFeign = memberLoginServiceFeign;
    }

    @RequestMapping("qq/auth")
    public String qqAuth(HttpServletRequest request) {
        try {
            String authorizeURL = new Oauth().getAuthorizeURL(request);
            return Constants.REDIRECT_INDEX + authorizeURL;
        } catch (QQConnectException e) {
            e.printStackTrace();
            return Constants.ERROR_500_FTL;
        }
    }

    @RequestMapping("qq/login/back")
    public String qqLoginBack(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        try {
            AccessToken accessToken = (new Oauth()).getAccessTokenByRequest(request);
            if (null == accessToken) return Constants.ERROR_500_FTL;

            String token = accessToken.getAccessToken();
            if (StringUtils.isEmpty(token)) return Constants.ERROR_500_FTL;

            OpenID openID = new OpenID(token);
            String userOpenID = openID.getUserOpenID();
            if (StringUtils.isEmpty(userOpenID)) return Constants.ERROR_500_FTL;

            BaseResponse<JSONObject> byOpenId = qqAuthFeign.findByOpenId(userOpenID);
            if (!isSuccess(byOpenId)) return Constants.ERROR_500_FTL;

            if (byOpenId.getCode().equals(Constants.HTTP_RES_CODE_203)) {
                UserInfo userInfo = new UserInfo(token, userOpenID);
                UserInfoBean userInfoBean = userInfo.getUserInfo();
                if (null == userInfoBean) return Constants.ERROR_500_FTL;
                request.setAttribute("avatarURL100", userInfoBean.getAvatar().getAvatarURL100());
                session.setAttribute(Constants.LOGIN_QQ_OPEN_ID, userOpenID);
                return Constants.MEMBER_QQ_LOGIN;
            }

            JSONObject data = byOpenId.getData();
            CookieUtils.setCookie(request, response, Constants.LOGIN_PC_TOKEN_COOKIE_NAME, data.getString("token"));
            return Constants.REDIRECT_INDEX;
        } catch (Exception e) {
            e.printStackTrace();
            return Constants.ERROR_500_FTL;
        }
    }

    @RequestMapping("qq/joint/login")
    public String qqJointLogin(@ModelAttribute("loginVO") LoginVO loginVO, Model model, HttpServletRequest request,
                               HttpServletResponse response, HttpSession session) {
        String qqOpenId = session.getAttribute(Constants.LOGIN_QQ_OPEN_ID).toString();
        if (StringUtils.isEmpty(qqOpenId)) return Constants.ERROR_500_FTL;

        UserLoginInputDTO userLoginInputDTO = BeanUtils.voToDto(loginVO, UserLoginInputDTO.class);
        userLoginInputDTO.setQqOpenId(qqOpenId);
        userLoginInputDTO.setLoginType(Constants.MEMBER_LOGIN_TYPE_PC);
        userLoginInputDTO.setDeviceInfo(webBrowserInfo(request));
        BaseResponse<JSONObject> login = memberLoginServiceFeign.login(userLoginInputDTO);
        if (!isSuccess(login)) {
            setErrorMsg(model, login.getMsg());
            return Constants.MEMBER_QQ_LOGIN;
        }
        CookieUtils.setCookie(request, response, Constants.LOGIN_PC_TOKEN_COOKIE_NAME, login.getData().getString("token"));
        return Constants.REDIRECT_INDEX;
    }

}
