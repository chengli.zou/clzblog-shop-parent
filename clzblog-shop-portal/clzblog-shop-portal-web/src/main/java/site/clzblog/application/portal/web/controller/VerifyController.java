package site.clzblog.application.portal.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.web.utils.RandomValidateCodeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class VerifyController {
    private final RandomValidateCodeUtils utils;

    @Autowired
    public VerifyController(RandomValidateCodeUtils utils) {
        this.utils = utils;
    }

    /**
     * 生成验证码
     */
    @RequestMapping(value = "get/verify")
    public void getVerify(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("image/jpeg");// 设置相应类型,告诉浏览器输出的内容为图片
            response.setHeader("Pragma", "No-cache");// 设置响应头信息，告诉浏览器不要缓存此内容
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);
            utils.getRandCode(request, response);// 输出验证码图片方法
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
