package site.clzblog.application.portal.web.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;
import site.clzblog.application.portal.web.controller.req.vo.LoginVO;
import site.clzblog.application.portal.web.feign.MemberLoginServiceFeign;
import site.clzblog.application.web.base.BaseWebController;
import site.clzblog.application.web.constants.Constants;
import site.clzblog.application.web.utils.CookieUtils;
import site.clzblog.application.web.utils.RandomValidateCodeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController extends BaseWebController {

    private final RandomValidateCodeUtils randomValidateCodeUtils;
    private final MemberLoginServiceFeign memberLoginServiceFeign;

    @Autowired
    public LoginController(RandomValidateCodeUtils randomValidateCodeUtils, MemberLoginServiceFeign memberLoginServiceFeign) {
        this.randomValidateCodeUtils = randomValidateCodeUtils;
        this.memberLoginServiceFeign = memberLoginServiceFeign;
    }

    @GetMapping("/login.html")
    public String getLogin() {
        return Constants.MEMBER_LOGIN_PAGE;
    }

    @GetMapping("login")
    public String login() {
        return getLogin();
    }

    @PostMapping("login")
    public String postLogin(@ModelAttribute("loginVO") LoginVO loginVO, Model model,
                            HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Boolean verify = randomValidateCodeUtils.checkVerify(loginVO.getGraphicCode(), session);

        if (!verify) {
            setErrorMsg(model, "Graphic code incorrect!");
            return Constants.MEMBER_LOGIN_PAGE;
        }

        UserLoginInputDTO userLoginInputDTO = BeanUtils.voToDto(loginVO, UserLoginInputDTO.class);
        userLoginInputDTO.setLoginType(Constants.MEMBER_LOGIN_TYPE_PC);
        userLoginInputDTO.setDeviceInfo(webBrowserInfo(request));

        BaseResponse<JSONObject> login = memberLoginServiceFeign.login(userLoginInputDTO);

        if (!isSuccess(login)) {
            setErrorMsg(model, login.getMsg());
            return Constants.MEMBER_LOGIN_PAGE;
        }

        CookieUtils.setCookie(request, response, Constants.LOGIN_PC_TOKEN_COOKIE_NAME, login.getData().getString("token"));

        return Constants.REDIRECT_INDEX;
    }

    @RequestMapping("exit")
    public String exit(HttpServletRequest request) {
        String token = CookieUtils.getCookieValue(request, Constants.LOGIN_PC_TOKEN_COOKIE_NAME);

        if (!StringUtils.isEmpty(token)) {
            BaseResponse<JSONObject> delToken = memberLoginServiceFeign.delToken(token);
            if (isSuccess(delToken)) return Constants.REDIRECT_INDEX;
        }

        return Constants.ERROR_500_FTL;
    }
}
