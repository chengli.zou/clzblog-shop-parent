package site.clzblog.application.portal.web.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.member.output.dto.UserOutputDTO;
import site.clzblog.application.portal.web.feign.MemberServiceFeign;
import site.clzblog.application.web.base.BaseWebController;
import site.clzblog.application.web.constants.Constants;
import site.clzblog.application.web.utils.CookieUtils;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController extends BaseWebController {
    private final MemberServiceFeign memberServiceFeign;

    @Autowired
    public IndexController(MemberServiceFeign memberServiceFeign) {
        this.memberServiceFeign = memberServiceFeign;
    }

    @RequestMapping("/")
    public String slash(HttpServletRequest request, Model model) {
        String token = CookieUtils.getCookieValue(request, Constants.LOGIN_PC_TOKEN_COOKIE_NAME, true);
        if (!StringUtils.isEmpty(token)) {
            BaseResponse<UserOutputDTO> userInfo = memberServiceFeign.getUserInfo(token);
            if (isSuccess(userInfo) && null != userInfo.getData()) {
                String mobile = userInfo.getData().getMobile();
                mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
                model.addAttribute("headMobile", mobile);
            }
        }
        return Constants.INDEX;
    }

    @RequestMapping(Constants.INDEX)
    public String index(HttpServletRequest request, Model model) {
        return slash(request, model);
    }

    @RequestMapping(Constants.INDEX + ".html")
    public String indexHtml(HttpServletRequest request, Model model) {
        return slash(request, model);
    }
}
