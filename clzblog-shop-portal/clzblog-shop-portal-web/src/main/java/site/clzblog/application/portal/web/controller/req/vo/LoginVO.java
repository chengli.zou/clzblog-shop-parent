package site.clzblog.application.portal.web.controller.req.vo;

import lombok.Data;

@Data
public class LoginVO {
    private String mobile;
    private String password;
    private String graphicCode;
}
