package site.clzblog.application.portal.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.member.service.MemberLoginService;

@FeignClient("app-clzblog-member")
public interface MemberLoginServiceFeign extends MemberLoginService {
}
