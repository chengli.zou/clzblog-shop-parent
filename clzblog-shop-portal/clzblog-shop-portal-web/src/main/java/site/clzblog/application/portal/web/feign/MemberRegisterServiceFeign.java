package site.clzblog.application.portal.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.member.service.MemberRegisterService;

@FeignClient("app-clzblog-member")
public interface MemberRegisterServiceFeign extends MemberRegisterService {
}
