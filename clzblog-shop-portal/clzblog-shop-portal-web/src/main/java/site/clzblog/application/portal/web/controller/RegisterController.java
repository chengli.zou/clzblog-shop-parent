package site.clzblog.application.portal.web.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.BeanUtils;
import site.clzblog.application.member.input.dto.UserInputDTO;
import site.clzblog.application.portal.web.controller.req.vo.RegisterVO;
import site.clzblog.application.portal.web.feign.MemberRegisterServiceFeign;
import site.clzblog.application.web.base.BaseWebController;
import site.clzblog.application.web.constants.Constants;
import site.clzblog.application.web.utils.RandomValidateCodeUtils;

import javax.servlet.http.HttpSession;
import java.util.Objects;

@Controller
public class RegisterController extends BaseWebController {
    private final MemberRegisterServiceFeign memberRegisterServiceFeign;

    private final RandomValidateCodeUtils randomValidateCodeUtils;

    @Autowired
    public RegisterController(MemberRegisterServiceFeign memberRegisterServiceFeign, RandomValidateCodeUtils randomValidateCodeUtils) {
        this.memberRegisterServiceFeign = memberRegisterServiceFeign;
        this.randomValidateCodeUtils = randomValidateCodeUtils;
    }

    @GetMapping("register")
    public String getRegister() {
        return Constants.MEMBER_REGISTER_PAGE;
    }

    @GetMapping("register.html")
    public String register() {
        return getRegister();
    }


    @PostMapping("register")
    public String postRegister(@ModelAttribute("registerVO") @Validated RegisterVO registerVO, BindingResult bindingResult,
                               Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            String message = Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage();
            setErrorMsg(model, message);
            return Constants.MEMBER_REGISTER_PAGE;
        }

        Boolean checkVerify = randomValidateCodeUtils.checkVerify(registerVO.getGraphicCode(), session);
        if (!checkVerify) {
            setErrorMsg(model, "Graphic code incorrect!");
            return Constants.MEMBER_REGISTER_PAGE;
        }

        UserInputDTO userInputDTO = BeanUtils.voToDto(registerVO, UserInputDTO.class);
        BaseResponse<JSONObject> register = memberRegisterServiceFeign.register(userInputDTO, registerVO.getRegisterCode());
        if (!isSuccess(register)) {
            setErrorMsg(model, register.getMsg());
            return Constants.MEMBER_REGISTER_PAGE;
        }

        return Constants.MEMBER_LOGIN_PAGE;
    }
}
