package site.clzblog.application.portal.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession()
public class SessionConfig {
    @Value("${redis.hostname:118.25.67.74}")
    String hostName;
    @Value("${redis.port:6379}")
    int port;
    @Value("${redis.password:zouchengli}")
    String passWord;

    @Bean
    public JedisConnectionFactory connectionFactory() {
        JedisConnectionFactory connection = new JedisConnectionFactory();
        connection.setPort(port);
        connection.setHostName(hostName);
        connection.setPassword(passWord);
        return connection;
    }

}
