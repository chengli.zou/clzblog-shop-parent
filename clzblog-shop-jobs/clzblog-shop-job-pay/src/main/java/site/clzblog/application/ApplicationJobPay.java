package site.clzblog.application;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableApolloConfig
@SpringBootApplication
public class ApplicationJobPay {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationJobPay.class, args);
    }
}
