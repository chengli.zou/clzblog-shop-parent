package site.clzblog.application.common.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {
    private final StringRedisTemplate stringRedisTemplate;

    @Autowired
    public RedisUtils(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public StringRedisTemplate getStringRedisTemplate() {
        return stringRedisTemplate;
    }

    public Boolean setNx(String key, String value, Long timeout) {
        if (timeout != null) stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
        return stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }

    public void setList(String key, List<String> listToken) {
        stringRedisTemplate.opsForList().leftPushAll(key, listToken);
    }

    /**
     * 存放string类型
     *
     * @param key     key
     * @param data    数据
     * @param timeout 超时间
     */
    public void setString(String key, String data, Long timeout) {
        stringRedisTemplate.opsForValue().set(key, data);
        if (timeout != null) stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);

    }

    /**
     * 存放string类型
     *
     * @param key  key
     * @param data 数据
     */
    public void setString(String key, String data) {
        setString(key, data, null);
    }

    /**
     * 根据key查询string类型
     *
     * @param key
     * @return
     */
    public String getString(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 根据对应的key删除key
     *
     * @param key
     */
    public Boolean delKey(String key) {
        return stringRedisTemplate.delete(key);
    }

    /**
     * Enable redis transaction support
     */
    public void begin() {
        stringRedisTemplate.setEnableTransactionSupport(true);
        stringRedisTemplate.multi();
    }

    /**
     * Commit redis transaction
     */
    public void exec() {
        stringRedisTemplate.exec();
    }

    /**
     * Rollback redis transaction
     */
    public void discard() {
        stringRedisTemplate.discard();
    }
}
