package site.clzblog.application.common.twitter.util;

import site.clzblog.application.common.twitter.SnowflakeIdWorker;

public class SnowflakeIdUtil {
    private static SnowflakeIdWorker snowflakeIdWorker;

    static {
        snowflakeIdWorker = new SnowflakeIdWorker(1, 1);
    }

    public static String nextId() {
        return String.valueOf(snowflakeIdWorker.nextId());
    }
}
