package site.clzblog.application.common.base;

import site.clzblog.application.common.constants.Constants;

public class BaseApiService<T> {
    protected BaseResponse<T> setResultError(Integer code, String msg) {
        return setResult(code, msg, null);
    }

    protected BaseResponse<T> setResultError(String msg) {
        return setResult(Constants.HTTP_RES_CODE_500, msg, null);
    }

    protected BaseResponse<T> setResultSuccess(T data) {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, data);
    }

    protected BaseResponse<T> setResultSuccess(String msg, T data) {
        return setResult(Constants.HTTP_RES_CODE_200, msg, data);
    }

    public BaseResponse<T> setResultSuccess() {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, null);
    }

    protected BaseResponse<T> setResultSuccess(String msg) {
        return setResult(Constants.HTTP_RES_CODE_200, msg, null);
    }

    private BaseResponse<T> setResult(Integer code, String msg, T data) {
        return new BaseResponse<>(code, msg, data);
    }

    protected Boolean toDaoResult(int result) {
        return result > 0;
    }

    protected Boolean isSuccess(BaseResponse<?> response) {
        if (null == response) return false;
        return response.getCode().equals(Constants.HTTP_RES_CODE_200);
    }
}
