package site.clzblog.application.common.error;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends BaseApiService<JSONObject> {
    /*private final KafkaSender<JSONObject> kafkaSender;

    @Autowired
    public GlobalExceptionHandler(KafkaSender<JSONObject> kafkaSender) {
        this.kafkaSender = kafkaSender;
    }*/

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public BaseResponse<JSONObject> exceptionHandler(Exception e) {
        JSONObject errorJson = new JSONObject();
        JSONObject logJson = new JSONObject();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logJson.put("request_time", df.format(new Date()));
        logJson.put("error_info", e);
        errorJson.put("request_error", logJson);
        //kafkaSender.send(errorJson);
        return setResultError(String.format("System error!%s", e.toString()));
    }
}
