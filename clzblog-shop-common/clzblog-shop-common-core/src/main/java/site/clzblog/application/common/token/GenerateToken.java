package site.clzblog.application.common.token;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.core.utils.RedisUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class GenerateToken {
    private final RedisUtils redisUtils;

    @Autowired
    public GenerateToken(RedisUtils redisUtils) {
        this.redisUtils = redisUtils;
    }

    /**
     * 生成令牌
     *
     * @param keyPrefix  令牌key前缀
     * @param redisValue redis存放的值
     * @return 返回token
     */
    public String createToken(String keyPrefix, String redisValue) {
        return createToken(keyPrefix, redisValue, null);
    }

    /**
     * 生成令牌
     *
     * @param keyPrefix  令牌key前缀
     * @param redisValue redis存放的值
     * @param time       有效期
     * @return 返回token
     */
    public String createToken(String keyPrefix, String redisValue, Long time) {
        if (StringUtils.isEmpty(redisValue)) throw new NullPointerException("Redis value cannot be empty");

        String token = keyPrefix + UUID.randomUUID().toString().replace("-", "");

        redisUtils.setString(token, redisValue, time);
        return token;
    }

    /**
     * 根据token获取redis中的value值
     *
     * @param token
     * @return
     */
    public String getToken(String token) {
        if (StringUtils.isEmpty(token)) return null;
        return redisUtils.getString(token);
    }

    /**
     * 移除token
     *
     * @param token
     * @return
     */
    public Boolean removeToken(String token) {
        if (StringUtils.isEmpty(token)) return null;
        return redisUtils.delKey(token);
    }

    public void createTokens(String keyPrefix, String redisKey, Long tokenQuantity) {
        redisUtils.setList(redisKey, getTokens(keyPrefix, tokenQuantity));
    }

    public List<String> getTokens(String keyPrefix, Long tokenQuantity) {
        List<String> listToken = new ArrayList<>();
        for (int i = 0; i < tokenQuantity; i++) {
            listToken.add(String.format("%s%s", keyPrefix, UUID.randomUUID().toString().replace("-", "")));
        }
        return listToken;
    }

    public String getTokensByKey(String key) {
        return redisUtils.getStringRedisTemplate().opsForList().leftPop(key);
    }
}
