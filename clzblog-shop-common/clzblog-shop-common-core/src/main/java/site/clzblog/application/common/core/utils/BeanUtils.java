package site.clzblog.application.common.core.utils;

public class BeanUtils {

    public static <DO> DO dtoToDo(Object dtoEntity, Class<DO> doClass) {
        if (dtoEntity == null || doClass == null) return null;
        try {
            DO newInstance = doClass.newInstance();
            org.springframework.beans.BeanUtils.copyProperties(dtoEntity, newInstance);
            return newInstance;
        } catch (Exception e) {
            return null;
        }
    }

    public static <DTO> DTO doToDto(Object doEntity, Class<DTO> dtoClass) {
        return getDTO(doEntity, dtoClass);
    }

    public static <DTO> DTO voToDto(Object voEntity, Class<DTO> dtoClass) {
        return getDTO(voEntity, dtoClass);
    }

    private static <DTO> DTO getDTO(Object voEntity, Class<DTO> dtoClass) {
        if (voEntity == null || dtoClass == null) return null;
        try {
            DTO newInstance = dtoClass.newInstance();
            org.springframework.beans.BeanUtils.copyProperties(voEntity, newInstance);
            return newInstance;
        } catch (Exception e) {
            return null;
        }
    }

}
