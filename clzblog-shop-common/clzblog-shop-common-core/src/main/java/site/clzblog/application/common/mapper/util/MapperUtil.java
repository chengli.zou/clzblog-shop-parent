package site.clzblog.application.common.mapper.util;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.List;

public class MapperUtil {
    static MapperFactory mapperFactory;

    static {
        mapperFactory = new DefaultMapperFactory.Builder().build();
    }

    public static <S, T> List<T> mapAsList(Iterable<S> source, Class<T> target) {
        return mapperFactory.getMapperFacade().mapAsList(source, target);

    }
}
