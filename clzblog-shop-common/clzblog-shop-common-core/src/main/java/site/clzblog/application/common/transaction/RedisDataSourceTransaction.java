package site.clzblog.application.common.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import site.clzblog.application.common.core.utils.RedisUtils;

@Component
@Scope(ConfigurableListableBeanFactory.SCOPE_PROTOTYPE)
public class RedisDataSourceTransaction {
    private final RedisUtils redisUtils;
    private final DataSourceTransactionManager dataSourceTransactionManager;

    @Autowired
    public RedisDataSourceTransaction(RedisUtils redisUtils,DataSourceTransactionManager dataSourceTransactionManager) {
        this.redisUtils = redisUtils;
        this.dataSourceTransactionManager = dataSourceTransactionManager;
    }

    /**
     * Begin transaction use default propagation behavior
     * @return
     */
    public TransactionStatus begin() {
        redisUtils.begin();
        return dataSourceTransactionManager.getTransaction(new DefaultTransactionAttribute());
    }

    /**
     * Commit transaction
     * @param status Transaction propagation behavior
     */
    public void commit(TransactionStatus status) {
        if (null == status) throw new NullPointerException("Transaction status is null");
        dataSourceTransactionManager.commit(status);
    }

    /**
     * Rollback transaction
     * @param status Transaction propagation behavior
     */
    public void rollback(TransactionStatus status) {
        if (null == status) throw new NullPointerException("Transaction status is null");
        dataSourceTransactionManager.rollback(status);
    }
}
