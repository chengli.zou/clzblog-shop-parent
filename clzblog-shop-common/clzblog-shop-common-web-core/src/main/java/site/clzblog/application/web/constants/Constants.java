package site.clzblog.application.web.constants;

public interface Constants extends site.clzblog.application.common.constants.Constants {
    String INDEX = "index";
    String REDIRECT_INDEX = "redirect:/";
    String MEMBER_QQ_LOGIN = "member/qq/login";
    String LOGIN_QQ_OPEN_ID = "qq_openid";
    String MEMBER_LOGIN_PAGE = "member/login";
    String MEMBER_REGISTER_PAGE = "member/register";
    String LOGIN_PC_TOKEN_COOKIE_NAME="login.pc.token";
    String ERROR_500_FTL = "500.ftl";
}
