package site.clzblog.application.web.base;

import nl.bitwalker.useragentutils.Browser;
import nl.bitwalker.useragentutils.UserAgent;
import nl.bitwalker.useragentutils.Version;
import org.springframework.ui.Model;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;

import javax.servlet.http.HttpServletRequest;

public class BaseWebController {
    protected static final String ERROR_500_FTL = "500";

    protected Boolean isSuccess(BaseResponse<?> baseResp) {
        if (baseResp == null) return false;
        return baseResp.getCode().equals(Constants.HTTP_RES_CODE_200);
    }

    protected void setErrorMsg(Model model, String errorMsg) {
        model.addAttribute("error", errorMsg);
    }

    protected String webBrowserInfo(HttpServletRequest request) {
        Browser browser = UserAgent.parseUserAgentString(request.getHeader("User-Agent")).getBrowser();
        Version version = browser.getVersion(request.getHeader("User-Agent"));
        return browser.getName() + "/" + version.getVersion();
    }

}
