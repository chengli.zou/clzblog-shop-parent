package site.clzblog.application.pay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PymtTransLogEntity {
    private Integer id;
    private String syncLog;
    private String asyncLog;
    private Integer channelId;
    private String transactionId;
    private Integer revision;
    private String createdBy;
    private Date createdTime;
    private String updatedBy;
    private Date updatedTime;
}
