package site.clzblog.application.zuul.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SwaggerDocJson {
    private String name;
    private String location;
    private String version;
}
