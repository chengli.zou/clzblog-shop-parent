package site.clzblog.application.order.entity;

import lombok.Data;

import java.util.Date;

@Data
public class OrderEntity {
    private Long id;
    // 订单名称
    private String name;
    // 订单时间
    private Date orderCreateTime;
    // 下单金额
    private Double orderMoney;
    // 订单状态
    private int orderState;
    // 商品id
    private Long commodityId;
}
