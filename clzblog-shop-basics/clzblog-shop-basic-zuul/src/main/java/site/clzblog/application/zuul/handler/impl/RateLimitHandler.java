package site.clzblog.application.zuul.handler.impl;

import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;
import site.clzblog.application.zuul.builder.GatewayBuilder;
import site.clzblog.application.zuul.handler.GatewayHandler;
import site.clzblog.application.zuul.handler.base.BaseHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RateLimitHandler extends BaseHandler implements GatewayHandler {
    public RateLimitHandler(GatewayBuilder gatewayBuilder) {
        this.gatewayBuilder = gatewayBuilder;
    }

    @Override
    public void service(RequestContext context, String ipAddress, HttpServletRequest request, HttpServletResponse response) {
        gatewayBuilder.rateLimiter(context, request);
        nextHandlerService(context, ipAddress, request, response);
    }
}
