package site.clzblog.application.zuul.handler.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Aspect
@Component
public class AopLogger {
    private static final String LOG_PREFIX = "Chain of responsibility handler";

    @Around("execution(* site.clzblog.application.zuul.handler.impl.*.*(..))")
    public Object around(ProceedingJoinPoint pjp) {
        Object result = null;
        long before, after;
        String className = pjp.getSignature().toShortString();
        className = className.substring(0, className.indexOf("."));
        before = System.nanoTime();
        try {
            log.info("{} {} started executing", LOG_PREFIX, className);
            result = pjp.proceed();
                log.info("{} {} finished executed", LOG_PREFIX, className);
        } catch (Throwable throwable) {
            log.info("{} {} occur exception : {}", LOG_PREFIX, className, throwable);
            throwable.printStackTrace();
        }
        after = System.nanoTime();
        log.info("{} {} consume millis {} ms", LOG_PREFIX, className, TimeUnit.NANOSECONDS.toMillis(after - before));
        return result;
    }
}
