package site.clzblog.application;

import com.alibaba.fastjson.JSONArray;
import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import site.clzblog.application.zuul.entity.SwaggerDocJson;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableSwagger2Doc
@EnableEurekaClient
@EnableZuulProxy
@EnableApolloConfig
@EnableFeignClients
@MapperScan(basePackages = "site.clzblog.application.zuul.mapper")
public class ApplicationZuul {
    @ApolloConfig
    Config config;

    @Value("${clzblog.zuul.swagger.document.config.json}")
    String swaggerDocument;

    public static void main(String[] args) {
        SpringApplication.run(ApplicationZuul.class, args);
    }

    @Primary
    @Component
    class DocumentationConfig implements SwaggerResourcesProvider {
        @Override
        public List<SwaggerResource> get() {
            config.addChangeListener(changeEvent -> get());
            return resources();
        }

        List<SwaggerResource> resources() {
            List<SwaggerResource> resources = new ArrayList<>();
            List<SwaggerDocJson> docJsons = JSONArray.parseArray(swaggerDocument, SwaggerDocJson.class);
            docJsons.forEach(doc -> resources.add(getSwaggerResource(doc.getName(), doc.getLocation(), doc.getVersion())));
            return resources;
        }

        private SwaggerResource getSwaggerResource(String name, String location, String version) {
            SwaggerResource resource = new SwaggerResource();
            resource.setName(name);
            resource.setLocation(location);
            resource.setSwaggerVersion(version);
            return resource;
        }
    }
}
