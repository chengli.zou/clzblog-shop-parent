package site.clzblog.application.zuul.handler.client;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.core.utils.SpringContextUtil;
import site.clzblog.application.zuul.handler.GatewayHandler;
import site.clzblog.application.zuul.mapper.GatewayHandlerMapper;
import site.clzblog.application.zuul.mapper.entity.GatewayHandlerEntity;

@Component
public class ResponsibilityClient extends BaseApiService<JSONObject> {
    private final GatewayHandlerMapper gatewayHandlerMapper;

    public ResponsibilityClient(GatewayHandlerMapper gatewayHandlerMapper) {
        this.gatewayHandlerMapper = gatewayHandlerMapper;
    }

    private GatewayHandler gatewayHandler;

    public GatewayHandler getHandler() {
        if (null != gatewayHandler) return gatewayHandler;

        GatewayHandlerEntity firstHandler = gatewayHandlerMapper.getFirstHandler();
        if (firstHandler == null) return null;

        String handlerId = firstHandler.getHandlerId();
        if (StringUtils.isEmpty(handlerId)) return null;

        GatewayHandler firstGatewayHandler = SpringContextUtil.getBean(handlerId, GatewayHandler.class);

        String nextHandlerId = firstHandler.getNextHandlerId();

        GatewayHandler temp = firstGatewayHandler;
        while (!StringUtils.isEmpty(nextHandlerId)) {
            GatewayHandlerEntity nextHandler = gatewayHandlerMapper.getByHandlerId(nextHandlerId);
            if (null == nextHandler) break;
            nextHandlerId = nextHandler.getNextHandlerId();

            GatewayHandler handler = SpringContextUtil.getBean(nextHandler.getHandlerId(), GatewayHandler.class);
            temp.setNextHandler(handler);
            temp = handler;
        }

        this.gatewayHandler = firstGatewayHandler;

        return firstGatewayHandler;
    }

    public BaseResponse<JSONObject> refreshGatewayHandler() {
        this.gatewayHandler = null;
        return setResultSuccess("Refreshed");
    }
}
