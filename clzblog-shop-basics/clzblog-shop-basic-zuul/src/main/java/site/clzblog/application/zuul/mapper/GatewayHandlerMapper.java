package site.clzblog.application.zuul.mapper;

import org.apache.ibatis.annotations.Select;
import site.clzblog.application.zuul.mapper.entity.GatewayHandlerEntity;

public interface GatewayHandlerMapper {
    @Select("SELECT handler_name AS handlerName,handler_id AS handlerId,prev_handler_id AS prevHandlerId,next_handler_id AS nextHandlerId,is_open AS isOpen FROM gateway_handler WHERE is_open = '1' and (prev_handler_id is null or prev_handler_id = '')")
    GatewayHandlerEntity getFirstHandler();

    @Select("SELECT handler_name AS handlerName,handler_id AS handlerId,prev_handler_id AS prevHandlerId,next_handler_id AS nextHandlerId,is_open AS isOpen FROM gateway_handler WHERE is_open = '1' and handler_id=#{handlerId}")
    GatewayHandlerEntity getByHandlerId(String handlerId);
}
