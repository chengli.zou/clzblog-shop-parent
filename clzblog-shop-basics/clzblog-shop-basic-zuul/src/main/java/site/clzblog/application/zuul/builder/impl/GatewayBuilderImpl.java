package site.clzblog.application.zuul.builder.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.RateLimiter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.common.constants.Constants;
import site.clzblog.application.common.sign.SignUtils;
import site.clzblog.application.common.token.GenerateToken;
import site.clzblog.application.zuul.builder.GatewayBuilder;
import site.clzblog.application.zuul.feign.AuthorizationServiceFeign;
import site.clzblog.application.zuul.mapper.BlackListMapper;
import site.clzblog.application.zuul.mapper.entity.BlackList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class GatewayBuilderImpl extends BaseApiService<JSONObject> implements GatewayBuilder {
    private final AuthorizationServiceFeign authorizationServiceFeign;

    private final BlackListMapper blackListMapper;

    private RateLimiter rateLimiter = RateLimiter.create(1);


    @Autowired
    public GatewayBuilderImpl(BlackListMapper blackListMapper, AuthorizationServiceFeign authorizationServiceFeign) {
        this.blackListMapper = blackListMapper;
        this.authorizationServiceFeign = authorizationServiceFeign;
    }

    @Override
    public Boolean blackListBlock(RequestContext requestContext, String ipAddress, HttpServletResponse response) {
        BlackList blacklist = blackListMapper.findBlacklist(ipAddress);
        if (blacklist != null) {
            error(requestContext, String.format("Ip -> %s Insufficient access rights", ipAddress));
            return false;
        }
        response.setHeader("ipAddress", ipAddress);
        return true;
    }

    @Override
    public Boolean parameterVerify(RequestContext requestContext, String ipAddress, HttpServletRequest request) {
        Map<String, String> verifyMap = SignUtils.toVerifyMap(request.getParameterMap(), false);
        String verify = SignUtils.verify(verifyMap);
        if (!verify.equals(Constants.OK)) {
            error(requestContext, String.format("Access ip -> %s Signature %s", ipAddress, verify));
            return false;
        }
        return true;
    }

    @Override
    public Boolean apiAuthority(RequestContext requestContext, HttpServletRequest request) {
        String path = request.getServletPath();
        if (path.length() >= 8 && !path.substring(0, 7).equals("/public")) return true;

        String accessToken = request.getParameter("accessToken");
        if (StringUtils.isEmpty(accessToken)) {
            error(requestContext, "Access token cannot be empty");
            return false;
        }

        BaseResponse<JSONObject> info = authorizationServiceFeign.getAppInfo(accessToken);
        if (!isSuccess(info)) {
            error(requestContext, info.getMsg());
            return false;
        }

        return true;
    }

    @Override
    public void rateLimiter(RequestContext requestContext, HttpServletRequest request) {

        String path = request.getServletPath();
        if (path.length() >= 7 && !path.substring(0, 6).equals("/spike")) return;

        boolean acquire = rateLimiter.tryAcquire(0, TimeUnit.SECONDS);
        if (!acquire) {
            error(requestContext, "Too many queues currently Please try again later");
        }

    }

    private void error(RequestContext ctx, String errorMsg) {
        ctx.setResponseStatusCode(Constants.HTTP_RES_CODE_401);
        ctx.setSendZuulResponse(false);
        ctx.setResponseBody(JSON.toJSONString(setResultError(Constants.HTTP_RES_CODE_401, errorMsg)));
    }

}
