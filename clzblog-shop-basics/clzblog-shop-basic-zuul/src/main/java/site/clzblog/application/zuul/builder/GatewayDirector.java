package site.clzblog.application.zuul.builder;

import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class GatewayDirector {
    private final GatewayBuilder gatewayBuilder;

    public GatewayDirector(GatewayBuilder gatewayBuilder) {
        this.gatewayBuilder = gatewayBuilder;
    }

    public void direct(RequestContext context, String ipAddress, HttpServletRequest request, HttpServletResponse response) {
        if (!gatewayBuilder.blackListBlock(context, ipAddress, response)) return;
        if (!gatewayBuilder.parameterVerify(context, ipAddress, request)) return;
        gatewayBuilder.apiAuthority(context, request);
    }
}
