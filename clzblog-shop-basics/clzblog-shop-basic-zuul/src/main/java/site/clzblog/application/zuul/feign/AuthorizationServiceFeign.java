package site.clzblog.application.zuul.feign;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.authorize.service.AuthorizationService;

@FeignClient("app-clzblog-authorize")
public interface AuthorizationServiceFeign extends AuthorizationService {
}
