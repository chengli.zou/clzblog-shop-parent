package site.clzblog.application.zuul.handler;

import com.netflix.zuul.context.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface GatewayHandler {
    void service(RequestContext context, String ipAddress, HttpServletRequest request, HttpServletResponse response);

    void setNextHandler(GatewayHandler gatewayHandler);
}
