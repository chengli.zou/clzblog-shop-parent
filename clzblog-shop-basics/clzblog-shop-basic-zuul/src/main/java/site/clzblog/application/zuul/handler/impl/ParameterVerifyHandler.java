package site.clzblog.application.zuul.handler.impl;

import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import site.clzblog.application.zuul.builder.GatewayBuilder;
import site.clzblog.application.zuul.handler.GatewayHandler;
import site.clzblog.application.zuul.handler.base.BaseHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class ParameterVerifyHandler extends BaseHandler implements GatewayHandler {

    public ParameterVerifyHandler(GatewayBuilder gatewayBuilder) {
        this.gatewayBuilder = gatewayBuilder;
    }

    @Override
    public void service(RequestContext context, String ipAddress, HttpServletRequest request, HttpServletResponse response) {
        Boolean parameterVerify = gatewayBuilder.parameterVerify(context, ipAddress, request);
        if (parameterVerify) nextHandlerService(context, ipAddress, request, response);
    }
}
