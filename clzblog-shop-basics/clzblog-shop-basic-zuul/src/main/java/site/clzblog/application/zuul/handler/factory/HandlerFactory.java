package site.clzblog.application.zuul.handler.factory;

import site.clzblog.application.common.core.utils.SpringContextUtil;
import site.clzblog.application.zuul.handler.GatewayHandler;

public class HandlerFactory {
    public static GatewayHandler getHandler() {
        GatewayHandler handler = SpringContextUtil.getBean("blackListBlockHandler", GatewayHandler.class);

        GatewayHandler handler1 = SpringContextUtil.getBean("parameterVerifyHandler", GatewayHandler.class);
        handler.setNextHandler(handler1);

        GatewayHandler handler2 = SpringContextUtil.getBean("apiAuthorityHandler", GatewayHandler.class);
        handler1.setNextHandler(handler2);

        return handler;
    }
}
