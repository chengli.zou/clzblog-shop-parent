package site.clzblog.application.zuul.builder;

import com.netflix.zuul.context.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface GatewayBuilder {
    Boolean blackListBlock(RequestContext requestContext, String ipAddress, HttpServletResponse response);

    Boolean parameterVerify(RequestContext requestContext, String ipAddress, HttpServletRequest request);

    Boolean apiAuthority(RequestContext requestContext, HttpServletRequest request);

    void rateLimiter(RequestContext requestContext, HttpServletRequest request);
}
