package site.clzblog.application.zuul.handler.base;

import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.context.RequestContext;
import site.clzblog.application.common.base.BaseApiService;
import site.clzblog.application.zuul.builder.GatewayBuilder;
import site.clzblog.application.zuul.handler.GatewayHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseHandler extends BaseApiService<JSONObject> {
    protected GatewayBuilder gatewayBuilder;

    private GatewayHandler gatewayHandler;

    public void setNextHandler(GatewayHandler gatewayHandler) {
        this.gatewayHandler = gatewayHandler;
    }

    protected void nextHandlerService(RequestContext context, String ipAddress, HttpServletRequest request, HttpServletResponse response) {
        if (null != gatewayHandler) gatewayHandler.service(context, ipAddress, request, response);
    }
}
