package site.clzblog.application.zuul.mapper;

import org.apache.ibatis.annotations.Select;
import site.clzblog.application.zuul.mapper.entity.BlackList;

public interface BlackListMapper {
    @Select("select id as id ,ip_address as ipAddress,restriction_type  as restrictionType, availability  as availability from clzblog_black_list where  ip_address =#{ipAddress} and  restriction_type='1'")
    BlackList findBlacklist(String ipAddress);
}
