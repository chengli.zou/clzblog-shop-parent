package site.clzblog.application.zuul.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.zuul.handler.client.ResponsibilityClient;

@RestController
public class GatewayController {
    private final ResponsibilityClient responsibilityClient;

    public GatewayController(ResponsibilityClient responsibilityClient) {
        this.responsibilityClient = responsibilityClient;
    }

    @PostMapping("refresh-gateway-handler")
    public BaseResponse<JSONObject> refreshGatewayHandler() {
        return responsibilityClient.refreshGatewayHandler();
    }
}
