package site.clzblog.application.sso.server.feigh;

import org.springframework.cloud.openfeign.FeignClient;
import site.clzblog.application.member.service.MemberAppService;

@FeignClient("app-clzblog-member")
public interface MemberAppServiceFeign extends MemberAppService {
}
