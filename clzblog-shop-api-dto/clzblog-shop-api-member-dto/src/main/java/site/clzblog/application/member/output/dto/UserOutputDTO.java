package site.clzblog.application.member.output.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "User information")
public class UserOutputDTO {
    @ApiModelProperty(value = "User id")
    private Long userId;
    @ApiModelProperty(value = "Mobile phone number")
    private String mobile;
    @ApiModelProperty(value = "Email")
    private String email;
    @ApiModelProperty(value = "Password")
    private String password;
    @ApiModelProperty(value = "User name")
    private String username;
    @ApiModelProperty(value = "User gender")
    private char gender;
    @ApiModelProperty(value = "User age")
    private int age;
    @ApiModelProperty(value = "Create time")
    private Date createTime;
    @ApiModelProperty(value = "Update time")
    private Date updateTime;
    @ApiModelProperty(value = "Account is available 1:normal 0:freeze")
    private char isAvailable;
    @ApiModelProperty(value = "User avatar")
    private String userAvatar;
    @ApiModelProperty(value = "QQ open id")
    private String qqOpenId;
    @ApiModelProperty(value = "Wechat open id")
    private String wechatOpenId;
}
