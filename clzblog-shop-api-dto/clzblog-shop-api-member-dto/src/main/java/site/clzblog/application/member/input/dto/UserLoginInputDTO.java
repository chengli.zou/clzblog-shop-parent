package site.clzblog.application.member.input.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "User login params")
public class UserLoginInputDTO {
    @ApiModelProperty("Mobile phone number")
    private String mobile;
    @ApiModelProperty("Password")
    private String password;
    @ApiModelProperty("Login type")
    private String loginType;
    @ApiModelProperty("Device information")
    private String deviceInfo;
    @ApiModelProperty("QQ open id")
    private String qqOpenId;
}
