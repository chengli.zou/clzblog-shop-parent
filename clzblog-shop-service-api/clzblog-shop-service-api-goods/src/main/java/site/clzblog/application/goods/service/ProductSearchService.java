package site.clzblog.application.goods.service;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.goods.output.dto.ProductOutputDTO;

import java.util.List;

public interface ProductSearchService {
    @GetMapping("search")
    BaseResponse<List<ProductOutputDTO>> search(String name, Pageable pageable);
}
