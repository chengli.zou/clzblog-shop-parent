package site.clzblog.application.member.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;

public interface QQAuthService {

    @RequestMapping("find/by/{open-id}")
    BaseResponse<JSONObject> findByOpenId(@PathVariable("open-id") String openId) throws Exception;
}
