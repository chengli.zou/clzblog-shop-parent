package site.clzblog.application.member.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;
import site.clzblog.application.member.output.dto.UserOutputDTO;

@Api(tags = "Member application service interface")
public interface MemberAppService {
    @ApiOperation("According mobile phone number is exist")
    @ApiImplicitParam(paramType = "query", name = "mobile", dataType = "String", required = true, value = "User mobile phone number")
    @PostMapping("exist/mobile")
    BaseResponse<UserOutputDTO> existMobile(@RequestParam("mobile") String mobile);

    @ApiOperation("According token get user information")
    @ApiImplicitParam(paramType = "query", name = "token", dataType = "String", required = true, value = "token")
    @GetMapping("get/user/info")
    BaseResponse<UserOutputDTO> getUserInfo(@RequestParam("token") String token);

    @ApiOperation("Sso login")
    @PostMapping("/sso/login")
    BaseResponse<UserOutputDTO> ssoLogin(@RequestBody UserLoginInputDTO userLoginInputDTO);

    @ApiOperation("Test get user info")
    @GetMapping("test/get/user/info")
    BaseResponse testGetUserInfo();
}
