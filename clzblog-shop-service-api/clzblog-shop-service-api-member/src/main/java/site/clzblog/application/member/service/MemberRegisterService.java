package site.clzblog.application.member.service;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.member.input.dto.UserInputDTO;

@Api(tags = "Member register interface")
public interface MemberRegisterService {
    @PostMapping("register")
    @ApiOperation(value = "Member user register information interface")
    @ApiImplicitParam(name = "registerCode", paramType = "query", dataType = "String", required = true, value = "Register code")
    BaseResponse<JSONObject> register(@RequestBody UserInputDTO userEntity, @RequestParam("registerCode") String registerCode);
}
