package site.clzblog.application.member.service;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.member.input.dto.UserLoginInputDTO;

@Api(tags = "Member login service interface")
public interface MemberLoginService {
    @ApiOperation("Member user login information interface")
    @PostMapping("login")
    BaseResponse<JSONObject> login(@RequestBody UserLoginInputDTO userLoginInputDTO);

    @ApiOperation("Member user login information interface")
    @DeleteMapping("del/{token}")
    @ApiImplicitParam(paramType = "query", name = "token", dataType = "String", required = true, value = "token")
    BaseResponse<JSONObject> delToken(@PathVariable("token") String token);
}
