package site.clzblog.application.pay.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;

public interface PayContextService {
    @GetMapping("to/pay/html")
    BaseResponse<JSONObject> toPayHtml(@RequestParam("channelId") String channelId, @RequestParam("payToken") String payToken);
}
