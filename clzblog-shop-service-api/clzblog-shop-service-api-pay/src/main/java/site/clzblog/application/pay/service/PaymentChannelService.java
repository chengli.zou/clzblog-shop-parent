package site.clzblog.application.pay.service;

import org.springframework.web.bind.annotation.GetMapping;
import site.clzblog.application.pay.output.dto.PymtChlOutDTO;

import java.util.List;

public interface PaymentChannelService {
    @GetMapping("select-all")
    List<PymtChlOutDTO> selectAll();
}
