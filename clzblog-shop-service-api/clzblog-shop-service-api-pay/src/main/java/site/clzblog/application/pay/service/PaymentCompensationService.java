package site.clzblog.application.pay.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;

public interface PaymentCompensationService {
    @RequestMapping("payment-compensation/{payment-id}")
    BaseResponse<JSONObject> paymentCompensation(@PathVariable("payment-id") String paymentId);
}
