package site.clzblog.application.pay.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.pay.input.dto.CreatePayTokenInDTO;

public interface PaymentTransTokenService {
    @GetMapping("create-pay-token")
    BaseResponse<JSONObject> createPayToken(@Validated CreatePayTokenInDTO createPayTokenInDTO);
}
