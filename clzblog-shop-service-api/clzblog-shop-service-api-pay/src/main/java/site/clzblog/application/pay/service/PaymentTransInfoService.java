package site.clzblog.application.pay.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.pay.output.dto.PymtTransOutDTO;

public interface PaymentTransInfoService {
    @GetMapping("payment-trans-by/{token}")
    BaseResponse<PymtTransOutDTO> tokenByPaymentTrans(@PathVariable("token") String token);
}
