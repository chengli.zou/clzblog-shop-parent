package site.clzblog.application.spike.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;

public interface SpikeCommodityService {
    @RequestMapping("spike")
    BaseResponse<JSONObject> spike(@RequestParam("phone") String phone, @RequestParam("secKillId") Long secKillId);

    @RequestMapping("spike/add/token")
    BaseResponse<JSONObject> addSpikeToken(Long secKillId, Long tokenQuantity);
}
