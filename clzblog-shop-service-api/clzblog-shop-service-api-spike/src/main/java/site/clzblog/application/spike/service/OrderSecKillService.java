package site.clzblog.application.spike.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import site.clzblog.application.common.base.BaseResponse;

public interface OrderSecKillService {
    @RequestMapping("/get/order")
    BaseResponse<JSONObject> getOrder(String phone, Long secKillId);
}
