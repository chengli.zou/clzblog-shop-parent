package site.clzblog.application.wechat.service;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;

@Api(tags = "Verify code service interface")
public interface VerifyCodeService {
    @ApiOperation("According to phone number verify code token is true")
    @PostMapping("verify/wechat-code")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "phone", dataType = "String", required = true, value = "User phone number"),
            @ApiImplicitParam(paramType = "query", name = "wechatCode", dataType = "String", required = true, value = "Wechat code")
    })
    BaseResponse<JSONObject> verifyWechatCode(@RequestParam("phone") String phone, @RequestParam("wechatCode") String wechatCode);
}
