package site.clzblog.application.wechat.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.web.bind.annotation.GetMapping;
import site.clzblog.application.common.base.BaseResponse;
import site.clzblog.application.wechat.entity.AppEntity;

@Api(tags = "WeChat application service")
public interface WeChatAppService {
    @ApiImplicitParam("Get application we chat info")
    @GetMapping("get/app")
    BaseResponse<AppEntity> getApp();
}
