package site.clzblog.application.stock.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;

public interface StockService {
    @RequestMapping("inventory-reduction")
    BaseResponse inventoryReduction(@RequestParam("commodityId") Long commodityId);
}
