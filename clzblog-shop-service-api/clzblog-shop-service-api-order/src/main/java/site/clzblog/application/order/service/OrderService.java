package site.clzblog.application.order.service;

import org.springframework.web.bind.annotation.GetMapping;
import site.clzblog.application.common.base.BaseResponse;

public interface OrderService {
    @GetMapping("add-order-and-stock")
    BaseResponse addOrderAndStock(int i) throws Exception;

    @GetMapping("order-to-member-test-get-user-info")
    BaseResponse orderToMemberTestGetUserInfo();
}
