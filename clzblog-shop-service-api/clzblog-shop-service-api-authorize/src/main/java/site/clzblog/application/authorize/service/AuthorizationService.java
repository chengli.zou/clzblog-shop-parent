package site.clzblog.application.authorize.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import site.clzblog.application.common.base.BaseResponse;

public interface AuthorizationService {
    @GetMapping("apply-app-info")
    BaseResponse<JSONObject> applyAppInfo(@RequestParam("appName") String appName);

    @GetMapping("get/access-token")
    BaseResponse<JSONObject> getAccessToken(@RequestParam("appId") String appId, @RequestParam("appSecret") String appSecret);

    @GetMapping("get/app-info")
    BaseResponse<JSONObject> getAppInfo(@RequestParam("accessToken") String accessToken);
}
